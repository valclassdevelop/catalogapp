// Action Type
const TYPE = {
  ADD_CART: 'ADD_CART',
  ADD_HISTORY: 'ADD_HISTORY',
  DELETE_CART_ALL: 'DELETE_CART_ALL',
  DELETE_CART_ONE: 'DELETE_CART_ONE',
  DELETE_HISTORY_ONE: 'DELETE_HISTORY_ONE',
  DELETE_HISTORY_ALL: 'DELETE_HISTORY_ALL',
};

// Action Creator
function ADDCART({data}) {
  return {
    type: TYPE.ADD_CART,
    value: data,
  };
}

function ADDHISTORY({data}) {
  return {
    type: TYPE.ADD_HISTORY,
    value: data,
  };
}

function DELETECARTALL() {
  return {
    type: TYPE.DELETE_CART_ALL,
    value: [],
  };
}

function DELETECARTONE({id}) {
  return {
    type: TYPE.DELETE_CART_ONE,
    value: id,
  };
}

function DELETEHISTORYALL() {
  return {
    type: TYPE.DELETE_HISTORY_ALL,
    value: [],
  };
}

function DELETEHISTORYONE() {
  return {
    type: TYPE.DELETEHISTORYONE,
    value: [],
  };
}

// Export All Function and Data
export {
  TYPE,
  ADDCART,
  ADDHISTORY,
  DELETECARTALL,
  DELETECARTONE,
  DELETEHISTORYALL,
  DELETEHISTORYONE,
};
