import {TYPE} from './action';

const initialState = {
  cart: [],
  history: [],
  notif: [
    {
      _id: 'HJ232D',
      image: 'https://source.unsplash.com/random/?sofa',
      title: 'New product',
      description: 'Sofa dengan kualitas terbaik dan terbaru telah hadir!',
      label: 'New',
    },
    {
      _id: 'HJ233D',
      image: 'https://source.unsplash.com/random/?sofa',
      title: 'New product',
      description: 'Sofa dengan kualitas terbaik dan terbaru telah hadir!',
      label: 'New',
    },
    {
      _id: 'HJ234D',
      image: 'https://source.unsplash.com/random/?sofa',
      title: 'New product',
      description: 'Sofa dengan kualitas terbaik dan terbaru telah hadir!',
      label: 'New',
    },
    {
      _id: 'HJ235D',
      image: 'https://source.unsplash.com/random/?sofa',
      title: 'New product',
      description: 'Sofa dengan kualitas terbaik dan terbaru telah hadir!',
      label: 'New',
    },
    {
      _id: 'HJ236D',
      image: 'https://source.unsplash.com/random/?sofa',
      title: 'New product',
      description: 'Sofa dengan kualitas terbaik dan terbaru telah hadir!',
      label: 'New',
    },
  ],
};

const ProductReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case TYPE.ADD_CART:
      return {
        ...state,
        cart: [action.value, ...state.cart],
      };
    case TYPE.ADD_HISTORY:
      return {
        ...state,
        history: state.history.concat(action.value),
      };
    case TYPE.DELETE_CART_ALL:
      return {
        ...state,
        cart: action.value,
      };
    case TYPE.DELETE_CART_ONE:
      return {
        ...state,
        cart: state.cart.filter(val => val._id !== action.value),
      };
    case TYPE.DELETE_HISTORY_ONE:
      return {
        ...state,
        history: state.history.filter(val2 => val2._id !== action.value),
      };
    case TYPE.DELETE_HISTORY_ALL:
      return {
        ...state,
        history: action.value,
      };
    default:
      return state;
  }
};

export default ProductReducer;
