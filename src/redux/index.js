import {configureStore} from '@reduxjs/toolkit';
import ProductReducer from './product/product';

const store = configureStore({
  reducer: {
    product: ProductReducer,
  },
});

export default store;
