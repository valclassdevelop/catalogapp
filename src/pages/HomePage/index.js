import React, {useState} from 'react';
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  FlatList,
  RefreshControl,
  TouchableHighlight,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useSelector, useDispatch} from 'react-redux';
import {BgPlay, IcAngelRight, IcClose, Play} from '../../assets';
import ListProperty from '../../assets/JSON/ListProperty';
import MenuPrice from '../../assets/JSON/MenuPrice';
import {
  Gap,
  Header,
  TextBold,
  TextRegular,
  BottomSheet,
} from '../../components';
import ListCardHorizontal from '../../components/listCardHorizontal';
import {DELETECARTALL} from '../../redux/product/action';
import {colors, scaleSize, NumberFormatter} from '../../utils';

const HomePage = () => {
  const listMenu = [
    {name: 'All'},
    {name: 'Fruit'},
    {name: 'Vegetable'},
    {name: 'Meat'},
    {name: 'Drink'},
    {name: 'Biscuit'},
  ];

  const dispatch = useDispatch();

  const [refreshStatus, setRefreshStatus] = useState(false);
  const [active, setActive] = useState(listMenu[0].name);
  const [activeModal, setActiveModal] = useState(false);
  const [activeModalCart, setActiveModalCart] = useState(false);
  const [press, setPress] = useState(false);
  const [press2, setPress2] = useState(false);
  const [setDataDetailCart] = useState([]);
  const [dataDetail, setDataDetail] = useState({
    name: '',
    price: 0,
    image: '',
    description: '',
  });

  const {cart} = useSelector(state => state.product);
  console.log('cart:', cart);

  const onRefresh = () => {
    setRefreshStatus(false);
    dispatch(DELETECARTALL);
  };

  const onPress = value => {
    setActive(value);
  };

  const onDetailProduct = items => {
    setActiveModal(true);
    setDataDetail({
      name: items.name,
      price: items.price,
      image: items.image,
      description: items.description,
    });
    console.log(items);
  };

  const onCloseCart = () => {
    setActiveModal(false);
    setDataDetail({
      name: '',
      price: '',
      image: '',
      description: '',
    });
  };

  const onCloseModalCart = () => {
    setPress2(true);
    setTimeout(() => {
      setPress2(false);
    }, 500);
    setActiveModalCart(false);
    setDataDetailCart({
      name: '',
      price: '',
      image: '',
      description: '',
    });
  };

  const onCartModal = () => {
    setActiveModalCart(true);
  };

  // eslint-disable-next-line react/no-unstable-nested-components
  const CardProduct = props => {
    return (
      <TouchableOpacity activeOpacity={0.8} onPress={props.onCard}>
        <View style={styles.cardProduct}>
          <Image
            source={{
              uri: 'https://source.unsplash.com/random/?sofa',
            }}
            style={{width: 160, height: 170, position: 'absolute'}}
          />
        </View>
      </TouchableOpacity>
    );
  };

  const RemoveCart = _id => {
    console.log(_id);
    const ok = dispatch({
      type: 'DELETE_CART_ONE',
      value: _id,
    });
    if (ok) {
      console.log('success');
    } else {
      console.log('failed');
    }
  };

  // eslint-disable-next-line react/no-unstable-nested-components
  const ListCart = ({imageLink, title, price, _id}) => {
    console.log(title);
    return (
      <TouchableOpacity activeOpacity={0.8}>
        <View style={styles.cardCart}>
          <View style={styles.cardLeft}>
            <Image
              source={{
                uri: imageLink,
              }}
              style={{width: 64, height: 64, borderRadius: scaleSize(6)}}
            />
            <Gap width={5} />
            <View style={styles.cardLeftDesc}>
              <TextRegular
                text={title}
                type="Text Regular 16"
                color={colors.Black}
              />
              <TextBold
                text={NumberFormatter(price, '$ ')}
                type="Text Bold 16"
                color={colors.Black}
              />
            </View>
          </View>
          <View style={styles.cardRight}>
            <TouchableOpacity activeOpacity={0.5}>
              <IcClose
                width={18}
                height={18}
                stroke={colors.White}
                onPress={() => RemoveCart(_id)}
              />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const onPayment = () => {
    const ok = dispatch({type: 'ADD_HISTORY', value: cart});
    dispatch({type: 'DELETE_CART_ALL'});
    if (ok) {
      setActiveModalCart(false);
    }
  };

  return (
    <>
      <Header type="main" onPress={onCartModal} text="welcome" />
      <ScrollView
        showsHorizontalScrollIndicator={false}
        pagingEnabled={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshStatus}
            onRefresh={() => onRefresh()}
          />
        }>
        <View style={styles.container}>
          <StatusBar
            barStyle="light-content"
            backgroundColor={colors.primary}
          />
          <Gap height={6} />
          <View style={styles.wrapperBanner}>
            <ScrollView
              contentContainerStyle={styles.contentContainer}
              pagingEnabled={false}
              horizontal
              shownHorizontalScrollIndicator={false}>
              <Gap height={4} />
              <View style={styles.menuTop}>
                {listMenu.map((menu, index) => (
                  <View key={index}>
                    <TouchableOpacity activeOpacity={0.8}>
                      <Text
                        onPress={() => onPress(menu.name)}
                        style={styles.textBar(active, menu.name)}
                        name={menu.name}>
                        {menu.name}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ))}
              </View>
            </ScrollView>
          </View>
          <Gap height={20} />
          <View style={styles.bannerHome}>
            <View style={styles.bannerLeft}>
              <TextRegular
                text="Impermediet"
                color={colors.White}
                type="Text Regular 12"
              />
              <Gap height={6} />
              <TextBold
                text="Quality home goods"
                type="Title Bold 20"
                color={colors.White}
              />
            </View>
            <View style={styles.wrapperImageBanner}>
              <Image
                source={Play}
                style={{
                  width: 50,
                  zIndex: 50,
                  height: 50,
                  left: scaleSize(50),
                  top: scaleSize(30),
                  position: 'absolute',
                }}
              />
              <Image
                source={BgPlay}
                style={{width: 137, height: 124, flex: 1}}
              />
            </View>
          </View>
          <Gap height={20} />
          <View style={styles.wrapperTitleProduct}>
            <TextBold
              text="Property"
              color={colors.White}
              type="Title Bold 18"
            />
            <TouchableOpacity activeOpacity={0.8} style={styles.btnAngle}>
              <IcAngelRight width={12} height={12} />
            </TouchableOpacity>
          </View>
          <Gap height={20} />
          <View styles={styles.wrappperCards}>
            <ScrollView horizontal howsHorizontalScrollIndicator={false}>
              <FlatList
                nestedScrollEnabled
                data={ListProperty}
                renderItem={({item}) => {
                  return (
                    <CardProduct
                      text={item.name}
                      price={item.price}
                      imageLink={item.image}
                    />
                  );
                }}
                horizontal
              />
            </ScrollView>
            <Gap height={20} />
            <View style={styles.wrapperTitleProduct}>
              <TextBold
                text="All products"
                color={colors.White}
                type="Title Bold 18"
              />
              <TouchableOpacity activeOpacity={0.8} style={styles.btnAngle}>
                <IcAngelRight width={12} height={12} />
              </TouchableOpacity>
            </View>
            <Gap height={20} />
            <FlatList
              contentContainerStyle={{paddingBottom: scaleSize(60)}}
              data={MenuPrice}
              renderItem={({item}) => {
                return (
                  <ListCardHorizontal
                    title={item.name}
                    price={item.price}
                    imageLink={item.image}
                    description={item.description}
                    onPressPrice={() => onDetailProduct(item)}
                  />
                );
              }}
            />
          </View>
        </View>

        {/* Modal detail product */}
        <BottomSheet isVisible={activeModal} onClose={onCloseCart}>
          <View style={styles.contentModal}>
            <FastImage
              source={{
                uri: dataDetail.image,
              }}
              style={{
                height: '70%',
                maxHeight: 210,
                width: '100%',
                borderRadius: 16,
              }}
            />
            <Gap height={12} />
            <TextBold
              text={dataDetail.name}
              type="Title Bold 20"
              color="black"
            />
            <Gap height={6} />
            <TextRegular
              text={dataDetail.description}
              type="Text Regular 12"
              color="grey"
            />
            <Gap height={20} />
            <TouchableHighlight
              onHideUnderlay={() => {
                setPress(false);
              }}
              onShowUnderlay={() => {
                setPress(true);
              }}
              activeOpacity={0.6}
              style={press ? styles.btnModalDetail2 : styles.btnModalDetail}
              onPress={() => onCloseCart()}>
              <TextBold
                style={styles.textBtnModal}
                text="Close"
                type="Text Bold 16"
                color={press ? 'white' : colors.Black}
              />
            </TouchableHighlight>
            <Gap height={20} />
          </View>
        </BottomSheet>

        {/* Modal cart */}
        <BottomSheet isVisible={activeModalCart} onClose={onCloseModalCart}>
          <View style={styles.contentModalHistory}>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={cart}
              renderItem={({item}) => {
                return (
                  <>
                    <ListCart
                      title={item.name}
                      price={item.price}
                      imageLink={item.image}
                      _id={item._id}
                    />
                    <Gap height={10} />
                  </>
                );
              }}
              ListFooterComponent={() => (
                <>
                  <Gap height={10} />

                  <View style={styles.wrapperBtnModal}>
                    <TouchableOpacity
                      activeOpacity={0.8}
                      style={
                        press2
                          ? styles.btnModalRed(cart)
                          : styles.btnModal(cart)
                      }
                      onPress={() => onCloseModalCart()}>
                      <TextBold
                        style={styles.textBtnModal}
                        text="Close"
                        type="Text Bold 16"
                        color={press2 ? colors.White : colors.Black}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      activeOpacity={cart?.length > 0 ? 0.8 : 1}
                      style={
                        cart?.length > 0 ? styles.btnModal2 : styles.btnModal2B
                      }
                      onPress={cart?.length > 0 ? () => onPayment() : null}>
                      <TextBold
                        style={styles.textBtnModal}
                        text="Checkout"
                        type="Text Bold 16"
                        color={cart?.length > 0 ? colors.White : 'gray'}
                      />
                    </TouchableOpacity>
                  </View>
                  <Gap height={20} />
                </>
              )}
            />
          </View>
        </BottomSheet>
      </ScrollView>
    </>
  );
};

export default HomePage;

const styles = StyleSheet.create({
  card: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    fontFamily: 'Poppins-Regular',
  },
  cardCart: {
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: scaleSize(12),
    flexDirection: 'row',
    fontFamily: 'Poppins-Regular',
  },
  cardLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontFamily: 'Poppins-Regular',
  },
  cardLeftDesc: {
    justifyContent: 'space-between',
    flexDirection: 'column',
    fontFamily: 'Poppins-Regular',
  },
  cardRight: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Poppins-Regular',
    backgroundColor: 'red',
    borderRadius: 99,
    width: scaleSize(28),
    height: scaleSize(28),
  },
  contentContainer: {
    alignSelf: 'baseline',
    marginBottom: 0,
    fontFamily: 'Poppins-Regular',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: scaleSize(20),
    backgroundColor: colors.primary,
    fontFamily: 'Poppins-Regular',
  },
  btnHomePage: {
    width: 200,
    elevation: 3,
    backgroundColor: 'blue',
    height: 40,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 'auto',
    textAlign: 'center',
    borderRadius: 6,
    alignItems: 'center',
    fontFamily: 'Poppins-Regular',
  },
  textBtn: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
  },
  textBar: (active, menu) => ({
    fontSize: scaleSize(14),
    paddingHorizontal: scaleSize(12),
    paddingVertical: scaleSize(6),
    alignItems: 'center',
    alignSelf: 'baseline',
    textAlign: 'center',
    fontFamily: 'Poppins-Black',
    color: active === menu ? 'white' : colors.White,
    backgroundColor: active === menu ? colors.purple : colors.primary,
    borderRadius: scaleSize(4),
    marginRight: scaleSize(10),
  }),
  wrappperCards: {
    paddingBottom: scaleSize(120),
    fontFamily: 'Poppins-Regular',
  },
  menuTop: {
    alignSelf: 'baseline',
    flexDirection: 'row',
    alignItems: 'center',
    fontFamily: 'Poppins-Regular',
  },
  bannerHome: {
    flexDirection: 'row',
    backgroundColor: colors.secondary,
    borderRadius: scaleSize(10),
    padding: scaleSize(14),
    justifyContent: 'space-between',
    alignItems: 'center',
    height: scaleSize(144),
    fontFamily: 'Poppins-Black',
  },
  bannerLeft: {
    flex: 1,
    fontFamily: 'Poppins-Regular',
  },
  wrapperBanner: {
    gap: scaleSize(20),
    fontFamily: 'Poppins-Regular',
  },
  wrapperImageBanner: {
    flex: 1,
    fontFamily: 'Poppins-Regular',
  },
  cardProduct: {
    width: scaleSize(157),
    height: scaleSize(117),
    borderRadius: scaleSize(10),
    padding: 0,
    marginRight: scaleSize(22),
    overflow: 'hidden',
    borderWidth: 2,
    borderColor: 'white',
    fontFamily: 'Poppins-Regular',
  },
  btnBag: {
    width: scaleSize(26),
    height: scaleSize(26),
    borderRadius: 999,
    position: 'absolute',
    left: scaleSize(8),
    top: scaleSize(8),
    backgroundColor: colors.purple,
    elevation: 3,
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Poppins-Regular',
  },
  description: {
    position: 'absolute',
    bottom: scaleSize(4),
    elevation: 4,
    left: scaleSize(6),
    fontFamily: 'Poppins-Regular',
  },
  wrapperTitleProduct: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    fontFamily: 'Poppins-Regular',
  },
  btnAngle: {
    width: 24,
    height: 24,
    borderRadius: 999,
    borderColor: colors.purple,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    fontFamily: 'Poppins-Regular',
  },
  cardAll: {
    width: scaleSize(40),
    height: scaleSize(40),
    borderRadius: scaleSize(999),
    bottom: scaleSize(50),
    right: scaleSize(50),
    zIndex: 33,
    elevation: 4,
    fontFamily: 'Poppins-Regular',
  },
  contentModal: {
    padding: 20,
    width: '100%',
    maxHeight: '60%',
    minHeight: '40%',
    alignSelf: 'baseline',
    fontFamily: 'Poppins-Regular',
  },
  wrapperBtnModal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontFamily: 'Poppins-Regular',
  },
  btnModalDetail: {
    width: '100%',
    height: 50,
    borderWidth: 1,
    borderColor: colors.Black,
    borderRadius: 99,
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Poppins-Bold',
  },
  btnModalDetail2: {
    width: '100%',
    height: 50,
    backgroundColor: colors.Black,
    borderRadius: 99,
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Poppins-Bold',
  },
  btnModal: cart => ({
    width: cart?.length > 0 ? '44%' : '100%',
    height: 50,
    borderWidth: 1,
    borderColor: colors.Black,
    borderRadius: 99,
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Poppins-Bold',
  }),
  btnModalRed: cart => ({
    width: cart?.length > 0 ? '44%' : '100%',
    height: 50,
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: 'red',
    borderRadius: 99,
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
  }),
  btnModal2: {
    width: '44%',
    height: 50,
    borderWidth: 2,
    borderColor: colors.purple,
    backgroundColor: colors.purple,
    borderRadius: 99,
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Poppins-Bold',
  },
  btnModal2B: {
    width: '44%',
    height: 50,
    borderWidth: 2,
    borderColor: 'gray',
    borderRadius: 99,
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'none',
    fontFamily: 'Poppins-Bold',
  },
  textBtnModal: {
    textAlign: 'justify',
    fontFamily: 'Poppins-Regular',
  },
  contentModalHistory: {
    padding: 10,
    paddingBottom: 20,
    width: '100%',
    alignSelf: 'baseline',
    maxHeight: 'auto',
    fontFamily: 'Poppins-Regular',
  },
});
