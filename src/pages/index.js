import HomePage from './HomePage';
import Notifications from './Notifications';
import Profile from './Profile';
import History from './History';
import Splash from './Splash';

export {HomePage, Notifications, Profile, History, Splash};
