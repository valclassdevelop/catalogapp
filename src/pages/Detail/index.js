import React, {useState} from 'react';
import FastImage from 'react-native-fast-image';
import {ScrollView, StatusBar, StyleSheet, View} from 'react-native/types';
import {IcHeartOutline} from '../../assets';
import {TextBold, TextRegular} from '../../components';
import {colors, NumberFormatter} from '../../utils';

const Detail = props => {
  const [refreshStatus, setRefreshStatus] = useState(false);
  const onRefresh = () => {
    setRefreshStatus(true);
  };

  return (
    <ScrollView
      showsHorizontalScrollIndicator={false}
      pagingEnabled={false}
      refreshControl={
        <refreshControl
          refreshing={refreshStatus}
          onRefresh={() => onRefresh()}
        />
      }>
      <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
      <View style={styles.container}>
        <View style={styles.topDetail}>
          <FastImage
            source={{
              uri: 'https://source.unsplash.com/random/?sofa',
            }}
          />
        </View>
        <TextBold text={props.name} type="Title Bold 24" />
        <View style={styles.wrapperPriceProduct}>
          <TextBold
            text={NumberFormatter(props.price, '$')}
            type="Title Bold 30"
            color={colors.White}
          />
          <View>
            <IcHeartOutline />
            <TextRegular text="34k" type="Text Regular 12" />
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  wrapperPriceProduct: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'baseline',
    justifyContent: 'space-between',
  },
});

export default Detail;
