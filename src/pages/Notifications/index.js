/* eslint-disable react/no-unstable-nested-components */
import React, {useState} from 'react';
import {
  FlatList,
  RefreshControl,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Dimensions,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {IcClose, IcFile} from '../../assets';
import {Gap, Header, TextBold, TextRegular} from '../../components';
import {colors, scaleSize} from '../../utils';

const Notifications = () => {
  const [statusNotifications] = useState(true);
  const [refreshStatus, setRefreshStatus] = useState(false);

  const dispatch = useDispatch();
  const {notif} = useSelector(state => state.product);
  console.log('notif :', notif);

  const onRefresh = () => {
    setRefreshStatus(false);
    dispatch({
      type: 'DELETE_CART_ALL',
    });
  };

  const ListNotifications = ({_id, title, description, image}) => {
    return (
      <>
        <TouchableOpacity activeOpacity={0.94}>
          <View style={styles.cardCart}>
            <View style={styles.cardLeft}>
              <Image
                source={{
                  uri: image,
                }}
                style={{
                  width: 320,
                  height: 200,
                  borderRadius: scaleSize(6),
                }}
              />
              <Gap width={5} />
            </View>
            <Gap height={12} />
            <View style={styles.cardLeftDesc}>
              <TextBold text={title} type="Text Bold 16" color={colors.Black} />
              <Gap height={10} />
              <TextRegular
                text={description}
                type="Text Regular 12"
                color={colors.Black}
              />
            </View>
            <View style={styles.cardRight}>
              <TouchableOpacity activeOpacity={0.5}>
                <IcClose width={18} height={18} stroke={colors.White} />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
        <Gap height={20} />
      </>
    );
  };

  return statusNotifications ? (
    <>
      <Header text="Profile" />
      <ScrollView
        showsHorizontalScrollIndicator={false}
        pagingEnabled={false}
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={refreshStatus}
            onRefresh={() => onRefresh()}
          />
        }>
        <Gap height={30} />
        <View style={styles.wrapperCardNotif}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={notif}
            renderItem={({item}) => {
              return (
                <ListNotifications
                  title={item.title}
                  description={item.description}
                  image={item.image}
                  label={item.label}
                  _id={item._id}
                />
              );
            }}
          />
          <Gap height={20} />
          <TouchableOpacity activeOpacity={0.8} style={styles.wrapperBtnRemove}>
            <TextRegular
              style={styles.btnRemove}
              text="Remove all"
              type="Text Regular 20"
              color={colors.White}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  ) : (
    <>
      <Header text="Profile" />
      <View style={styles.wrapperNotFound}>
        <IcFile width={60} height={60} />
        <Gap height={20} />
        <TextBold
          style={styles.titleNotFound}
          text="Not Found"
          color={colors.White}
          type="Title Bold 32"
        />
      </View>
    </>
  );
};

export default Notifications;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  wrapperNotFound: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    top: -20,
    height: Dimensions.get('window').height,
    backgroundColor: colors.primary,
  },
  cardCart: {
    justifyContent: 'space-between',
    paddingBottom: scaleSize(12),
    paddingTop: scaleSize(12),
    fontFamily: 'Poppins-Regular',
    backgroundColor: colors.White,
    borderRadius: scaleSize(12),
  },
  cardLeft: {
    flexDirection: 'row',
    width: '92%',
    marginLeft: 'auto',
    borderRadius: scaleSize(12),
    marginRight: 'auto',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontFamily: 'Poppins-Regular',
    overflow: 'hidden',
  },
  cardLeftDesc: {
    justifyContent: 'space-between',
    flexDirection: 'column',
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    fontFamily: 'Poppins-Regular',
  },
  cardRight: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Poppins-Regular',
    backgroundColor: 'red',
    borderRadius: 99,
    width: scaleSize(32),
    height: scaleSize(32),
    right: scaleSize(10),
    top: scaleSize(6),
    position: 'absolute',
  },
  wrapperCardNotif: {
    paddingHorizontal: scaleSize(20),
    paddingBottom: scaleSize(80),
  },
  btnRemove: {
    fontFamily: 'Poppins-Regular',
  },
  wrapperBtnRemove: {
    width: '100%',
    height: 48,
    marginBottom: scaleSize(30),
    borderRadius: 99,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
  },
});
