import {useNavigation} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Dimensions, Image, StyleSheet, View} from 'react-native';
import {IcLauncher} from '../../assets';
import {colors} from '../../utils';

const Splash = () => {
  const Navigation = useNavigation();
  setTimeout(() => {
    Navigation.navigate('HomePage');
  }, 1000);

  return (
    <View style={styles.container}>
      <Image source={IcLauncher} style={{width: 100, height: 100}} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: Dimensions.get('window').height,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Splash;
