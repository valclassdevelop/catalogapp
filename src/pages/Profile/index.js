import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  RefreshControl,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {FaceUser, IcAngelRight, IcSad} from '../../assets';
import {Gap, Header, TextBold, TextRegular} from '../../components';
import {colors, scaleSize} from '../../utils';

const Profile = () => {
  const [statusProfile] = useState(true);
  const [refreshStatus, setRefreshStatus] = useState(false);

  const dispatch = useDispatch();

  const onRefresh = () => {
    setRefreshStatus(false);
    dispatch({
      type: 'DELETE_CART_ALL',
    });
  };

  const onPresss = (d, navigation) => {
    navigation.navigate(d);
  };

  const menuProfile = [
    {title: 'My orders', desc: 'Ongoing to your home', link: 'History'},
    {title: 'Payment', desc: 'Via bank, Paypal, COD', link: 'Payment'},
    {
      title: 'Notification',
      desc: 'Read new notification',
      link: 'Notifications',
    },
    {title: 'Information', desc: 'Company, Contact, FAQ'},
  ];

  const CardMenuProfile = ({title, description, onPress}) => {
    return (
      <TouchableOpacity activeOpacity={0.9}>
        <View style={styles.cardMenuProfile} onPress={onPress}>
          <View style={styles.cardLeft}>
            <TextBold
              text={title}
              style={styles.titleCard}
              color={colors.Black}
              type="Title Bold 28"
            />
            <Gap height={10} />
            <TextRegular text={description} type="Text Regular 12" />
          </View>
          <View style={styles.btnCardMenuProfile}>
            <TouchableOpacity activeOpacity={0.6}>
              <IcAngelRight width={14} height={14} />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <Header text="Profile" />
      <ScrollView
        showsHorizontalScrollIndicator={false}
        pagingEnabled={false}
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={refreshStatus}
            onRefresh={() => onRefresh()}
          />
        }>
        <View style={styles.wrapperNotFound}>
          {statusProfile ? (
            <>
              <View style={styles.wrapperFoto}>
                <View style={styles.fotoUser}>
                  <Image source={FaceUser} style={{width: 85, height: 120}} />
                </View>
                <View style={styles.descUser}>
                  <TextBold
                    style={styles.username}
                    text="Fahreza Maliq"
                    type="Title Bold 28"
                    color={colors.White}
                  />
                  <Gap height={10} />
                  <TextRegular
                    text="fahreza@gmail.com"
                    color={colors.White}
                    type="Text Regular 12"
                  />
                </View>
              </View>
              <View style={styles.wrapperCardProfile}>
                <FlatList
                  data={menuProfile}
                  renderItem={({item}) => {
                    return (
                      <CardMenuProfile
                        title={item.title}
                        description={item.desc}
                        onPress={() => onPresss(item.link)}
                      />
                    );
                  }}
                />
              </View>
            </>
          ) : (
            <>
              <IcSad width={60} height={60} />
              <Gap height={20} />
              <TextBold
                text="Maintanence"
                color={colors.Black}
                type="Title Bold 32"
                style={styles.text}
              />
            </>
          )}
          <Gap height={20} />
          <TouchableOpacity activeOpacity={0.8} style={styles.btn}>
            <TextRegular
              text="Logout"
              type="Text Regular 18"
              color={colors.White}
              style={styles.textBtn}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 'auto',
    backgroundColor: colors.primary,
  },
  wrapperNotFound: {
    top: scaleSize(20),
    alignItems: 'center',
    height: 610,
    fontFamily: 'Poppins-Regular',
  },
  text: {
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
  },
  wrapperFoto: {
    marginTop: scaleSize(10),
    marginBottom: scaleSize(10),
    alignSelf: 'baseline',
    width: '100%',
    flexDirection: 'row',
    paddingLeft: scaleSize(20),
    alignItems: 'center',
    paddingBottom: scaleSize(20),
  },
  fotoUser: {
    width: scaleSize(100),
    height: scaleSize(100),
    backgroundColor: colors.White,
    borderRadius: 99,
    overflow: 'hidden',
    elevation: 3,
  },
  username: {
    width: 'auto',
    fontFamily: 'Poppins-Regular',
  },
  descUser: {
    marginLeft: scaleSize(20),
    fontFamily: 'Poppins-Regular',
  },
  cardMenuProfile: {
    width: Dimensions.get('window').width * 0.9,
    height: scaleSize(74),
    borderRadius: scaleSize(12),
    elevation: 3,
    backgroundColor: colors.White,
    paddingHorizontal: scaleSize(10),
    paddingVertical: scaleSize(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: scaleSize(22),
  },
  btnCardMenuProfile: {
    width: scaleSize(24),
    height: scaleSize(24),
    backgroundColor: colors.White,
    borderRadius: scaleSize(99),
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Poppins-Regular',
  },
  titleCard: {
    fontFamily: 'Poppins-Bold',
  },
  wrapperCardProfile: {
    alignSelf: 'baseline',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  btn: {
    width: '90%',
    bottom: scaleSize(42),
    position: 'absolute',
    height: scaleSize(50),
    borderRadius: scaleSize(99),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
  },
  textBtn: {
    fontFamily: 'Poppins-Regular',
  },
});
