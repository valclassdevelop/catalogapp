import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  RefreshControl,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {IcAngelRight, IcClose, IcFile} from '../../assets';
import {
  BottomSheet,
  Gap,
  Header,
  TextBold,
  TextRegular,
} from '../../components';
import {colors, NumberFormatter, scaleSize} from '../../utils';

const History = () => {
  const dispatch = useDispatch();

  const [refreshStatus, setRefreshStatus] = useState(false);

  const [valueDrop1, setValueDrop1] = useState('Popular');
  const [valueDrop2, setValueDrop2] = useState('Last post');

  const [activeDrop1, setActiveDrop1] = useState(false);
  const [activeDrop2, setActiveDrop2] = useState(false);
  const [active, setActive] = useState('Popular');
  const [active2, setActive2] = useState('Last post');

  const dataDrop1 = [
    {label: 'Popular', value: 'Popular'},
    {label: 'Recent', value: 'Recent'},
    {label: 'Trend', value: 'Trend'},
    {label: 'Best product', value: 'Best product'},
    {label: 'Branding', value: 'Branding'},
  ];

  const dataDrop2 = [
    {label: 'Last post', value: 'Last post'},
    {label: 'New product', value: 'New product'},
    {label: 'Old product', value: 'Old product'},
    {label: 'Second item', value: 'Second item'},
    {label: 'Free item', value: 'Free item'},
  ];

  const onCloseDrop1 = () => {
    setActiveDrop1(false);
  };

  const onCloseDrop2 = () => {
    setActiveDrop2(false);
  };

  const {history} = useSelector(state => state.product);

  const onPress = navigation => {
    navigation.navigate('Detail');
  };

  const onRefresh = () => {
    setRefreshStatus(false);
  };

  const onSelectDrop1 = d => {
    setActive(d);
    setValueDrop1(d);
  };

  const onSelectDrop2 = d => {
    setActive2(d);
    setValueDrop2(d);
  };

  const onDeleteHistoryOnly = _id => {
    console.log(_id);
    dispatch({
      type: 'DELETE_HISTORY_ONE',
      value: _id,
    });
    console.log('ok');
  };

  const onRemoveAll = () => {
    dispatch({
      type: 'DELETE_HISTORY_ALL',
    });
  };

  const CardHistory = ({image, name, price, description, onPress}) => (
    <ScrollView>
      <View style={styles.cardHistory}>
        <View style={styles.btnDelete}>
          <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
            <IcClose width={18} height={18} stroke={colors.White} />
          </TouchableOpacity>
        </View>
        <View style={styles.cardHistoryLeft}>
          <FastImage
            style={styles.imageHistory}
            resizeMode="cover"
            source={{
              uri: image,
            }}
          />
          <View style={styles.cardHistoryRight}>
            <TextRegular
              text={name}
              type="Title Regular 16"
              color={colors.White}
            />
            <Gap height={2} />
            <TextBold
              text={NumberFormatter(price, '$ ')}
              type="Text Bold 14"
              color={colors.White}
            />
          </View>
        </View>
        <Gap height={4} />
        <TextRegular
          style={styles.textDesc}
          text={description}
          type="Text Regular 14"
          color={colors.White}
        />
      </View>
    </ScrollView>
  );

  const navigation = useNavigation();
  const onBuy = () => {
    navigation.navigate('HomePage');
  };

  return (
    <>
      <Header type="main" text="welcome" />
      <ScrollView
        style={styles.wrapperScroll}
        showsHorizontalScrollIndicator={false}
        pagingEnabled={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshStatus}
            onRefresh={() => onRefresh()}
          />
        }>
        <View style={styles.container}>
          <StatusBar
            barStyle="light-content"
            backgroundColor={colors.primary}
          />
          <Gap height={6} />
          <View style={styles.wrapperDropdown}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.drop1}
              onPress={() => setActiveDrop1(true)}>
              <TextRegular
                text={valueDrop1}
                type="Text Regular 12"
                color={colors.White}
              />
              <TouchableOpacity
                style={styles.arrowDrop}
                activeOpacity={0.8}
                onPress={() => setActiveDrop1(true)}>
                <IcAngelRight width={12} height={12} stroke={colors.White} />
              </TouchableOpacity>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.drop2}
              onPress={() => setActiveDrop2(true)}>
              <TextRegular
                text={valueDrop2}
                type="Text Regular 12"
                color={colors.White}
              />
              <TouchableOpacity
                style={styles.arrowDrop}
                activeOpacity={0.8}
                onPress={() => setActiveDrop2(true)}>
                <IcAngelRight width={12} height={12} stroke={colors.White} />
              </TouchableOpacity>
            </TouchableOpacity>
          </View>
          <Gap height={20} />
          {history?.length ? (
            <View style={styles.listHistory}>
              <FlatList
                data={history}
                renderItem={({item}) => {
                  return (
                    <View key={item._id}>
                      <CardHistory
                        _id={item._id}
                        name={item.name}
                        price={item.price}
                        description={item.description}
                        image={item.image}
                        onPress={() => onDeleteHistoryOnly(item._id)}
                      />
                      <Gap height={20} />
                    </View>
                  );
                }}
              />
              <Gap height={20} />
              <TouchableOpacity
                onPress={() => onRemoveAll()}
                activeOpacity={0.8}
                style={styles.wrapperBtnRemove}>
                <TextRegular
                  style={styles.btnRemove}
                  text="Remove all"
                  type="Text Regular 20"
                  color={colors.White}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.wrapperNotFound}>
              <IcFile width={60} height={60} />
              <Gap height={20} />
              <TextBold
                style={styles.titleNotFound}
                text="Not Found"
                color={colors.White}
                type="Title Bold 32"
              />
              <Gap height={10} />
              <TouchableOpacity
                onPress={() => onBuy()}
                activeOpacity={0.8}
                style={styles.btnBuy}>
                <TextRegular
                  text="Buy now"
                  style={styles.textBtnBuy}
                  type="Text Regular 14"
                  color={colors.White}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
        {/* Modal dropdown1 */}
        <BottomSheet isVisible={activeDrop1} onClose={onCloseDrop1}>
          <View style={styles.wrapperDrop1}>
            {dataDrop1.map((data, index) => (
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.menuChild(active, data.label)}
                key={index}
                onPress={() => onSelectDrop1(data.label)}>
                <Text style={styles.textMenuChild(active, data.label)}>
                  {data.label}
                </Text>
              </TouchableOpacity>
            ))}
            <TouchableOpacity activeOpacity={0.8} style={styles.btnModalDrop1}>
              <TextRegular
                text="Close"
                type="Text Regular 14"
                color={colors.White}
              />
            </TouchableOpacity>
          </View>
        </BottomSheet>
        <BottomSheet isVisible={activeDrop2} onClose={onCloseDrop2}>
          <View style={styles.wrapperDrop1}>
            {dataDrop2.map((data, index) => (
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.menuChild2(active2, data.label)}
                key={index}
                onPress={() => onSelectDrop2(data.label)}>
                <Text style={styles.textMenuChild2(active2, data.label)}>
                  {data.label}
                </Text>
              </TouchableOpacity>
            ))}
            <TouchableOpacity activeOpacity={0.8} style={styles.btnModalDrop1}>
              <TextRegular
                text="Close"
                type="Text Regular 14"
                color={colors.White}
              />
            </TouchableOpacity>
          </View>
        </BottomSheet>
      </ScrollView>
    </>
  );
};

export default History;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    minHeight: Dimensions.get('window').height,
    flexDirection: 'column',
    padding: scaleSize(20),
    backgroundColor: colors.primary,
    fontFamily: 'Poppins-Regular',
  },
  imageHistory: {
    width: scaleSize(80),
    backgroundColor: 'red',
    height: scaleSize(80),
    borderRadius: scaleSize(8),
    marginRight: scaleSize(12),
    resizeMode: 'contain',
    fontFamily: 'Poppins-Regular',
  },
  wrapperScroll: {
    flex: 1,
    paddingBottom: scaleSize(120),
    fontFamily: 'Poppins-Regular',
  },
  wrapperDropdown: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 'auto',
    fontFamily: 'Poppins-Regular',
  },
  drop1: {
    width: '44%',
    height: 44,
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 99,
    backgroundColor: '#313131',
    paddingLeft: 12,
    fontFamily: 'Poppins-Regular',
  },
  drop2: {
    width: '44%',
    height: 44,
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 99,
    backgroundColor: '#313131',
    paddingLeft: 12,
    fontFamily: 'Poppins-Regular',
  },
  menuChild: (active, data) => ({
    borderWidth: 1,
    marginHorizontal: scaleSize(4),
    marginVertical: scaleSize(8),
    borderColor: 'green',
    paddingVertical: scaleSize(10),
    paddingHorizontal: scaleSize(6),
    elevation: active === data ? 3 : 0,
    alignSelf: 'baseline',
    borderRadius: scaleSize(99),
    backgroundColor: active === data ? 'green' : 'transparent',
  }),
  textMenuChild: (active, data) => ({
    fontWeight: active === data ? 'bold' : 'normal',
    fontSize: scaleSize(16),
    color: active === data ? colors.White : 'green',
  }),
  menuChild2: (active2, data) => ({
    borderWidth: 1,
    marginHorizontal: scaleSize(4),
    marginVertical: scaleSize(8),
    borderColor: 'green',
    paddingVertical: scaleSize(10),
    paddingHorizontal: scaleSize(6),
    elevation: active2 === data ? 3 : 0,
    alignSelf: 'baseline',
    borderRadius: scaleSize(99),
    backgroundColor: active2 === data ? 'green' : 'transparent',
  }),
  textMenuChild2: (active2, data) => ({
    fontWeight: active2 === data ? 'bold' : 'normal',
    fontSize: scaleSize(16),
    color: active2 === data ? colors.White : 'green',
  }),
  arrowDrop: {
    borderRadius: 32,
    padding: 2,
    width: scaleSize(20),
    height: scaleSize(20),
    backgroundColor: colors.purple,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: scaleSize(10),
    fontFamily: 'Poppins-Regular',
  },
  wrapperDrop1: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    padding: scaleSize(26),
    paddingBottom: scaleSize(44),
    width: '100%',
  },
  listHistory: {
    flex: 1,
    height: 'auto',
    paddingBottom: scaleSize(60),
    fontFamily: 'Poppins-Regular',
  },
  cardHistoryLeft: {
    flexDirection: 'row',
    alignSelf: 'baseline',
    fontFamily: 'Poppins-Regular',
  },
  cardHistory: {
    backgroundColor: '#414141',
    borderRadius: 16,
    elevation: 3,
    padding: scaleSize(20),
    fontFamily: 'Poppins-Regular',
  },
  cardHistoryRight: {
    alignSelf: 'baseline',
    fontFamily: 'Poppins-Regular',
  },
  textDesc: {
    marginTop: -12,
    fontFamily: 'Poppins-Regular',
  },
  btnDelete: {
    position: 'absolute',
    right: 20,
    top: 20,
    backgroundColor: 'red',
    borderRadius: 99,
    width: scaleSize(26),
    height: scaleSize(26),
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Poppins-Regular',
  },
  wrapperNotFound: {
    justifyContent: 'center',
    top: scaleSize(200),
    alignItems: 'center',
    fontFamily: 'Poppins-Regular',
  },
  btnRemove: {
    fontFamily: 'Poppins-Regular',
  },
  wrapperBtnRemove: {
    width: '100%',
    height: 48,
    marginBottom: scaleSize(30),
    borderRadius: 99,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
  },
  btnBuy: {
    width: scaleSize(120),
    height: scaleSize(36),
    borderRadius: scaleSize(99),
    backgroundColor: colors.purple,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBtnBuy: {
    fontFamily: 'Poppins-Regular',
  },
});
