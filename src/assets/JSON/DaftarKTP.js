const DataAgama = [
  'Islam',
  'Kristen',
  'Hindu',
  'Buddha',
  'Katolik',
  'Kong Hu Chu',
];

const DataStatus = ['Belum Kawin', 'Kawin', 'Cerai Hidup', 'Cerai Mati'];

const DataPekerjaan = [
  'Belum/ Tidak Bekerja',
  'Mengurus Rumah Tangga',
  'Pelajar/ Mahasiswa',
  'Pensiunan',
  'Pewagai Negeri Sipil',
  'Tentara Nasional Indonesia',
  'Kepolisisan RI',
  'Perdagangan',
  'Petani/ Pekebun',
  'Karyawan Swasta',
  'Karyawan BUMN',
];

const DataNegara = ['WNI', 'WNA'];

const DataProvinsi = [
  'Jawa Timur',
  'Jawa Barat',
  'Jawa Tengah',
  'Banten',
  'DKI Jakarta',
  'Maluku',
  'Papua',
  'Papua Barat',
];

const DataKota = [
  'Kab. Blitar',
  'Kab. Majalengka',
  'Kota Cirebon',
  'Kota Bandung',
  'Jakarta Barat',
  'Jakarta Timur',
  'Jakarta Selatan',
  'Tangerang',
];

const DataKecamatan = [
  'Nglegok',
  'Sumberjaya',
  'Jatiwangi',
  'Sindanglaut',
  'Nglegok',
  'Nglegok',
  'Nglegok',
  'Nglegok',
];

const DataDesa = [
  'Nglegok',
  'Panjalin Kidul',
  'Panjalin Lor',
  'Karangsembung',
  'Nglegok',
  'Nglegok',
  'Nglegok',
  'Nglegok',
];

export {
  DataAgama,
  DataStatus,
  DataPekerjaan,
  DataNegara,
  DataProvinsi,
  DataKota,
  DataKecamatan,
  DataDesa,
};
