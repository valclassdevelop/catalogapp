import React from 'react';
import {colors} from '../../utils';
import {IcMultifinance, IcWallet} from '../Icons';

export default [
  {
    title: 'E-money',
    image: <IcWallet />,
  },
  {
    title: 'Multifinance',
    image: <IcMultifinance color={colors.White} />,
  },
];
