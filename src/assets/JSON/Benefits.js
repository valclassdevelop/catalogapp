export default [
  {
    id: 1,
    title: 'NukuPay mudah digunakan',
    description:
      'Semakin mudah untuk melakukan transaksi karena aplikasi NukuPay sangat mudah untuk digunakan.',
  },
  {
    id: 2,
    title: 'Pendaftaran lebih mudah',
    description:
      'Proses pendaftaran di aplikasi NukuPay sangat mudah dan tidak memerlukan proses yang panjang.',
  },
  {
    id: 3,
    title: 'Banyak keuntungan di NukuPay',
    description:
      'Kamu akan mendapat banyak keuntungan dengan mendapat poin disetiap transaksi dan membagikan kode refferal.',
  },
  {
    id: 4,
    title: 'Bisa kirim saldo kesesama NukuPay',
    description:
      'NukuPay juga memiliki fitur yang dapat mengirimkan saldo ke sesama pengguna.',
  },
];
