export default [
  {
    id: 1,
    category: 'Populer',
  },
  {
    id: 2,
    category: 'Harian',
  },
  {
    id: 3,
    category: 'Tagihan',
  },
  {
    id: 4,
    category: 'Keuangan',
  },
  {
    id: 5,
    category: 'Transportasi',
  },
];
