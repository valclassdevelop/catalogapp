export default [
  {
    id: 1,
    message:
      'Apakah kamu baru mencoba masuk OVO di perangkat lain? Jangan bagikan kode jika ini bukan kamu.',
    isRead: false,
    createdAt: '03 November 2022',
  },
  {
    id: 2,
    message:
      'Apakah kamu baru mencoba masuk OVO di perangkat lain? Jangan bagikan kode jika ini bukan kamu.',
    isRead: false,
    createdAt: '03 November 2022',
  },
  {
    id: 3,
    message:
      'Apakah kamu baru mencoba masuk OVO di perangkat lain? Jangan bagikan kode jika ini bukan kamu.',
    isRead: true,
    createdAt: '03 November 2022',
  },
  {
    id: 4,
    message:
      'Apakah kamu baru mencoba masuk OVO di perangkat lain? Jangan bagikan kode jika ini bukan kamu.',
    isRead: false,
    createdAt: '04 November 2022',
  },
  {
    id: 5,
    message:
      'Apakah kamu baru mencoba masuk OVO di perangkat lain? Jangan bagikan kode jika ini bukan kamu.',
    isRead: true,
    createdAt: '04 November 2022',
  },
];
