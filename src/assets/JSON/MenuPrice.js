export default [
  {
    name: 'Tortor tincidunt',
    price: 1111,
    image: 'https://source.unsplash.com/random/?sofa',
    description:
      'Menurut Kamus Besar Bahasa Indonesia (KBBI) deskripsi merupakan pemaparan atau penggambaran dengan kata-kata secara jelas dan terperinci. Deskripsi juga diartikan sebagai uraian. Deskripsi berasal dari bahasa Latin describere yang artinya menggambarkan atau memberikan suatu hal',
  },
  {
    name: 'Tortor tincidunt',
    price: 2222,
    image: 'https://source.unsplash.com/random/?sofa',
    description:
      'Menurut Kamus Besar Bahasa Indonesia (KBBI) deskripsi merupakan pemaparan atau penggambaran dengan kata-kata secara jelas dan terperinci. Deskripsi juga diartikan sebagai uraian. Deskripsi berasal dari bahasa Latin describere yang artinya menggambarkan atau memberikan suatu hal',
  },
  {
    name: 'Tortor tincidunt',
    price: 3333,
    image: 'https://source.unsplash.com/random/?sofa',
    description:
      'Menurut Kamus Besar Bahasa Indonesia (KBBI) deskripsi merupakan pemaparan atau penggambaran dengan kata-kata secara jelas dan terperinci. Deskripsi juga diartikan sebagai uraian. Deskripsi berasal dari bahasa Latin describere yang artinya menggambarkan atau memberikan suatu hal',
  },
  {
    name: 'Tortor tincidunt',
    price: 4444,
    image: 'https://source.unsplash.com/random/?sofa',
    description:
      'Menurut Kamus Besar Bahasa Indonesia (KBBI) deskripsi merupakan pemaparan atau penggambaran dengan kata-kata secara jelas dan terperinci. Deskripsi juga diartikan sebagai uraian. Deskripsi berasal dari bahasa Latin describere yang artinya menggambarkan atau memberikan suatu hal',
  },
  {
    name: 'Tortor tincidunt',
    price: 5555,
    image: 'https://source.unsplash.com/random/?sofa',
    description:
      'Menurut Kamus Besar Bahasa Indonesia (KBBI) deskripsi merupakan pemaparan atau penggambaran dengan kata-kata secara jelas dan terperinci. Deskripsi juga diartikan sebagai uraian. Deskripsi berasal dari bahasa Latin describere yang artinya menggambarkan atau memberikan suatu hal',
  },
  {
    name: 'Tortor tincidunt',
    price: 6666,
    image: 'https://source.unsplash.com/random/?sofa',
    description:
      'Menurut Kamus Besar Bahasa Indonesia (KBBI) deskripsi merupakan pemaparan atau penggambaran dengan kata-kata secara jelas dan terperinci. Deskripsi juga diartikan sebagai uraian. Deskripsi berasal dari bahasa Latin describere yang artinya menggambarkan atau memberikan suatu hal',
  },
  {
    name: 'Tortor tincidunt',
    price: 7777,
    image: 'https://source.unsplash.com/random/?sofa',
    description:
      'Menurut Kamus Besar Bahasa Indonesia (KBBI) deskripsi merupakan pemaparan atau penggambaran dengan kata-kata secara jelas dan terperinci. Deskripsi juga diartikan sebagai uraian. Deskripsi berasal dari bahasa Latin describere yang artinya menggambarkan atau memberikan suatu hal',
  },
  {
    name: 'Tortor tincidunt',
    price: 8888,
    image: 'https://source.unsplash.com/random/?sofa',
    description:
      'Menurut Kamus Besar Bahasa Indonesia (KBBI) deskripsi merupakan pemaparan atau penggambaran dengan kata-kata secara jelas dan terperinci. Deskripsi juga diartikan sebagai uraian. Deskripsi berasal dari bahasa Latin describere yang artinya menggambarkan atau memberikan suatu hal',
  },
];
