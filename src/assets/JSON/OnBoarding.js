export default [
  {
    id: '1',
    title: 'Aplikasi NukuPay',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut tellus ante diam eget nulla sit proin.',
    image: require('../Dummy/OnBoarding1.png'),
    background: require('../Dummy/Background1.png'),
  },
  {
    id: '2',
    title: 'Pendaftaran Gratis',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut tellus ante diam eget nulla sit proin.',
    image: require('../Dummy/OnBoarding2.png'),
    background: require('../Dummy/Background2.png'),
  },
  {
    id: '3',
    title: 'Transaksi 24 Jam',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut tellus ante diam eget nulla sit proin.',
    image: require('../Dummy/OnBoarding3.png'),
    background: require('../Dummy/Background3.png'),
  },
];
