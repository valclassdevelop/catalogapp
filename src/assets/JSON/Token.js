export default [
  {
    id: 1,
    nominal: 5000,
    price: 6000,
  },
  {
    id: 2,
    nominal: 10000,
    price: 11000,
  },
  {
    id: 3,
    nominal: 12000,
    price: 13000,
  },
  {
    id: 4,
    nominal: 15000,
    price: 16000,
  },
  {
    id: 5,
    nominal: 20000,
    price: 21000,
  },
  {
    id: 6,
    nominal: 25000,
    price: 26000,
  },
  {
    id: 7,
    nominal: 30000,
    price: 31000,
  },
  {
    id: 8,
    nominal: 50000,
    price: 51000,
  },
];
