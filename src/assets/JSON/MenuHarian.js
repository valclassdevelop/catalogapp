import React from 'react';
import {colors} from '../../utils';
import {
  IcCall,
  IcData,
  IcPulsa,
  IcPulsaTransfer,
  IcToken,
  IcVoucher,
  IcWifiId,
} from '../Icons';

export default [
  {
    title: 'Pulsa',
    image: <IcPulsa fill={colors['DeepDenim-550']} color={colors.White} />,
  },
  {
    title: 'Paket Data',
    image: <IcData />,
  },
  {
    title: 'Token Listrik',
    image: <IcToken />,
  },
  {
    title: 'Voucher Games',
    image: <IcVoucher fill={colors['DeepDenim-550']} />,
  },
  {
    title: 'Paket SMS & Telepon',
    image: <IcCall fill={colors['DeepDenim-550']} color={colors.White} />,
  },
  {
    title: 'Pulsa Transfer',
    image: (
      <IcPulsaTransfer fill={colors['DeepDenim-550']} color={colors.White} />
    ),
  },
  {
    title: 'Wifi ID',
    image: <IcWifiId />,
  },
];
