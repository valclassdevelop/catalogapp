export default [
  {
    id: 1,
    name: 'Freedom Combo 1GB / 30 Hari',
    description:
      'Kuota utama 4GB+kuota malam 2GB\nNelpon sepuasnya ke sesama IM3 & nelpon...',
    price: 31000,
    status: 'available',
  },
  {
    id: 2,
    name: 'Freedom Combo 2GB / 30 Hari',
    description:
      'Kuota utama 4GB+kuota malam 2GB\nNelpon sepuasnya ke sesama IM3 & nelpon...',
    price: 31000,
    status: 'unavailable',
  },
  {
    id: 3,
    name: 'Freedom Combo 3GB / 30 Hari',
    description:
      'Kuota utama 4GB+kuota malam 2GB\nNelpon sepuasnya ke sesama IM3 & nelpon...',
    price: 31000,
    status: 'maintenance',
  },
  {
    id: 4,
    name: 'Freedom Combo 4GB / 30 Hari',
    description:
      'Kuota utama 4GB+kuota malam 2GB\nNelpon sepuasnya ke sesama IM3 & nelpon...',
    price: 31000,
    status: 'available',
  },
  {
    id: 5,
    name: 'Freedom Combo 5GB / 30 Hari',
    description:
      'Kuota utama 4GB+kuota malam 2GB\nNelpon sepuasnya ke sesama IM3 & nelpon...',
    price: 31000,
    status: 'available',
  },
  {
    id: 6,
    name: 'Freedom Combo 6GB / 30 Hari',
    description:
      'Kuota utama 4GB+kuota malam 2GB\nNelpon sepuasnya ke sesama IM3 & nelpon...',
    price: 31000,
    status: 'available',
  },
  {
    id: 7,
    name: 'Freedom Combo 7GB / 30 Hari',
    description:
      'Kuota utama 4GB+kuota malam 2GB\nNelpon sepuasnya ke sesama IM3 & nelpon...',
    price: 31000,
    status: 'available',
  },
  {
    id: 8,
    name: 'Freedom Combo 8GB / 30 Hari',
    description:
      'Kuota utama 4GB+kuota malam 2GB\nNelpon sepuasnya ke sesama IM3 & nelpon...',
    price: 31000,
    status: 'available',
  },
];
