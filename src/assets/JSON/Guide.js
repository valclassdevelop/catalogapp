export default [
  {
    id: 1,
    title: 'Cara Top up di NukuPay',
    description:
      'Semakin mudah untuk melakukan transaksi karena aplikasi NukuPay sangat mudah untuk digunakan.',
  },
  {
    id: 2,
    title: 'Cara Transaksi di NukuPay',
    description:
      'Proses pendaftaran di aplikasi NukuPay sangat mudah dan tidak memerlukan proses yang panjang.',
  },
  {
    id: 3,
    title: 'Cara Kirim Saldo di NukuPay',
    description:
      'Kamu akan mendapat banyak keuntungan dengan mendapat poin disetiap transaksi dan membagikan kode refferal.',
  },
];
