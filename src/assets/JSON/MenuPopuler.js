import React from 'react';
import {
  IcData,
  IcElectric,
  IcHealth,
  IcIndihome,
  IcKredit,
  IcPulsa,
  IcVoucher,
  IcWater,
} from '../Icons';
import {colors} from '../../utils';

export default [
  {
    title: 'Pulsa',
    image: <IcPulsa fill={colors['DeepDenim-550']} color={colors.White} />,
  },
  {
    title: 'Paket Data',
    image: <IcData />,
  },
  {
    title: 'PLN',
    image: <IcElectric />,
  },
  {
    title: 'Voucher Games',
    image: <IcVoucher fill={colors['DeepDenim-550']} />,
  },
  {
    title: 'Indihome',
    image: <IcIndihome color={colors.White} />,
  },
  {
    title: 'Air PDAM',
    image: <IcWater fill={colors['DeepDenim-550']} />,
  },
  {
    title: 'BPJS',
    image: <IcHealth />,
  },
  {
    title: 'Angsuran Kredit',
    image: <IcKredit fill={colors['DeepDenim-550']} color={colors.White} />,
    action: 'MoreServices',
  },
];
