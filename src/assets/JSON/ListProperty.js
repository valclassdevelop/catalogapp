export default [
  {
    name: 'Minimalist Sofa',
    price: '25',
    image: 'https://source.unsplash.com/random/?sofa',
  },
  {
    name: 'Standart Sofa',
    price: '215',
    image: 'https://source.unsplash.com/random/?sofa',
  },
  {
    name: 'Flexibel Sofa',
    price: '1545',
    image: 'https://source.unsplash.com/random/?sofa',
  },
  {
    name: 'ELectronic Sofa',
    price: '35',
    image: 'https://source.unsplash.com/random/?sofa',
  },
];
