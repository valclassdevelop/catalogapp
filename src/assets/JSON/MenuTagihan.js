import React from 'react';
import {colors} from '../../utils';
import {
  IcElectric,
  IcHealth,
  IcIndihome,
  IcInsurance,
  IcNaturalGas,
  IcPascabayar,
  IcTax,
  IcTelephone,
  IcWater,
} from '../Icons';

export default [
  {
    title: 'Tagihan Listrik',
    image: <IcElectric />,
  },
  {
    title: 'Indihome',
    image: <IcIndihome color={colors.White} />,
  },
  {
    title: 'Air PDAM',
    image: <IcWater fill={colors['DeepDenim-550']} />,
  },
  {
    title: 'BPJS',
    image: <IcHealth />,
  },
  {
    title: 'Asuransi',
    image: <IcInsurance color={colors.White} />,
  },
  {
    title: 'Telepon Kabel',
    image: <IcTelephone fill={colors['DeepDenim-550']} />,
  },
  {
    title: 'Pascabayar',
    image: <IcPascabayar color={colors.White} />,
  },
  {
    title: 'Pajak PBB',
    image: <IcTax color={colors.White} />,
  },
  {
    title: 'Gas Alam',
    image: <IcNaturalGas />,
  },
];
