export default [
  {
    id: '1',
    name: 'Info Umum',
    items: [
      {
        id: '1.1',
        name: 'Registrasi',
        items: [
          {
            id: '1.1.1',
            name: 'Bagaimana cara registrasi di NukuPay?',
            items: [],
          },
          {
            id: '1.1.2',
            name: 'Apa keuntungan registrasi di NukuPay?',
            items: [],
          },
        ],
      },
      {
        id: '1.2',
        name: 'Transaksi di NukuPay',
        items: [],
      },
    ],
  },
  {
    id: '2',
    name: 'Akun dan Pengaturan',
    items: [],
  },
  {
    id: '3',
    name: 'Upgrade ke Premium',
    items: [],
  },
  {
    id: '4',
    name: 'Top Up',
    items: [],
  },
  {
    id: '5',
    name: 'Transfer',
    items: [],
  },
  {
    id: '6',
    name: 'Tagihan dan Isi Ulang',
    items: [],
  },
];
