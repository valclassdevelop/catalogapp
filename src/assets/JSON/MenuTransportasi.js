import React from 'react';
import {IcToll} from '../Icons';

export default [
  {
    title: 'E-toll',
    image: <IcToll />,
  },
];
