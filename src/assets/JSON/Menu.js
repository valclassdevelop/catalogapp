import React from 'react';
import {
  IcData,
  IcElectric,
  IcHealth,
  IcIndihome,
  IcMore,
  IcPulsa,
  IcVoucher,
  IcWater,
} from '../Icons';
import {colors} from '../../utils';

export default [
  {
    title: 'Pulsa',
    image: <IcPulsa fill={colors['DeepDenim-550']} color={colors.White} />,
    action: 'Pulsa',
  },
  {
    title: 'Paket Data',
    image: <IcData />,
    action: 'PaketData',
  },
  {
    title: 'PLN',
    image: <IcElectric />,
    action: 'PLN',
  },
  {
    title: 'Voucher Games',
    image: <IcVoucher fill={colors['DeepDenim-550']} />,
  },
  {
    title: 'Indihome',
    image: <IcIndihome color={colors.White} />,
  },
  {
    title: 'Air PDAM',
    image: <IcWater fill={colors['DeepDenim-550']} />,
  },
  {
    title: 'BPJS',
    image: <IcHealth />,
  },
  {
    title: 'Lainnya',
    image: <IcMore />,
    action: 'MoreServices',
  },
];
