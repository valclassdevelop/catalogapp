export default [
  {
    id: 1,
    product: 'Indosat 5.000',
    image: require('../Dummy/Indosat.png'),
    price: 6000,
    status: 'Sukses',
    date: '9 November 2022',
    time: '12:00',
  },
  {
    id: 2,
    product: 'Telkomsel 5.000',
    image: require('../Dummy/Telkomsel.png'),
    price: 6000,
    status: 'Diproses',
    date: '9 November 2022',
    time: '12:00',
  },
  {
    id: 3,
    product: 'Axis 5.000',
    image: require('../Dummy/Axis.png'),
    price: 6000,
    status: 'Gagal',
    date: '9 November 2022',
    time: '12:00',
  },
];
