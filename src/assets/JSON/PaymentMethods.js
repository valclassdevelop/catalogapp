export default [
  {
    id: 1,
    category: 'Nukupay',
    items: [
      {
        icon: require('../Dummy/Nukupay.png'),
        name: 'Saldo NukuPay',
        amount: 750000,
      },
      {
        icon: require('../Dummy/Point.png'),
        name: 'Poin NukuPay',
        amount: 750,
      },
    ],
  },
  {
    id: 2,
    category: 'Transfer Bank (Verifikasi Otomatis)',
    items: [
      {
        icon: require('../Dummy/BRI.png'),
        name: 'Bank Rakyat Indonesia',
      },
      {
        icon: require('../Dummy/BCA.png'),
        name: 'Bank Central Asia',
      },
      {
        icon: require('../Dummy/BNI.png'),
        name: 'BNI',
      },
      {
        icon: require('../Dummy/Mandiri.png'),
        name: 'Mandiri',
      },
    ],
  },
  {
    id: 3,
    category: 'E-wallet (Verifikasi Manual)',
    items: [
      {
        icon: require('../Dummy/Dana.png'),
        name: 'Dana',
      },
      {
        icon: require('../Dummy/Linkaja.png'),
        name: 'LinkAja',
      },
    ],
  },
  {
    id: 4,
    category: 'Tunai Digerai Retail',
    items: [
      {
        icon: require('../Dummy/Alfamart.png'),
        name: 'Alfamart',
      },
      {
        icon: require('../Dummy/Indomaret.png'),
        name: 'Infomaret',
      },
    ],
  },
  {
    id: 5,
    category: 'Transfer Virtual Account',
    items: [
      {
        icon: require('../Dummy/BRI.png'),
        name: 'BRIVA',
      },
      {
        icon: require('../Dummy/BCA.png'),
        name: 'BCA Virtual Account',
      },
      {
        icon: require('../Dummy/BNI.png'),
        name: 'BNI Virtual Account',
      },
      {
        icon: require('../Dummy/Mandiri.png'),
        name: 'Mandiri Virtual Account',
      },
    ],
  },
];
