export default [
  {
    id: 1,
    product: 'Trx 5.085678901234',
    image: require('../Dummy/Indosat.png'),
    nominal: 6000,
    status: 'Uang Keluar',
    date: '9 November 2022',
    time: '12:00',
  },
  {
    id: 2,
    product: 'Trx PLN20.1234567890',
    image: require('../Dummy/PLN.png'),
    nominal: 20000,
    status: 'Uang Keluar',
    date: '9 November 2022',
    time: '12:00',
  },
  {
    id: 3,
    product: 'Deposit BRI - Transfer BRI',
    image: require('../Dummy/BRI.png'),
    nominal: 750123,
    status: 'Uang Masuk',
    date: '9 November 2022',
    time: '12:00',
  },
];
