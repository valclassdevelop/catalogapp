export default [
  {
    id: 1,
    nominal: 750123,
    image: require('../Dummy/BRI.png'),
    status: 'Sukses',
    date: '9 November 2022',
    time: '12:00',
  },
  {
    id: 2,
    nominal: 750123,
    image: require('../Dummy/BCA.png'),
    status: 'Diproses',
    date: '9 November 2022',
    time: '12:00',
  },
  {
    id: 3,
    nominal: 750123,
    image: '',
    status: 'Gagal',
    date: '9 November 2022',
    time: '12:00',
  },
];
