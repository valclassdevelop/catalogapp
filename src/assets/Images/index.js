import BgPlay from './bgplay.png';
import Play from './play.png';
import BgSofa1 from './BgSofa1.png';
import BgSofa2 from './BgSofa2.png';
import FaceUser from './FaceUser.png';
import IcLauncher from './IcLauncher.png';

export {BgPlay, Play, BgSofa1, BgSofa2, FaceUser, IcLauncher};
