// Chevron
import IcChevronUp from './IcChevronUp.svg';
import IcChevronDown from './IcChevronDown.svg';
import IcChevronLeft from './IcChevronLeft.svg';
import IcChevronRight from './IcChevronRight.svg';

// Arrow
import IcArrowUp from './IcArrowUp.svg';
import IcArrowDown from './IcArrowDown.svg';
import IcArrowLeft from './IcArrowLeft.svg';
import IcArrowRight from './IcArrowRight.svg';

// Alert
import IcError from './IcError.svg';
import IcWarning from './IcWarning.svg';
import IcErrorProduct from './IcErrorProduct.svg';
import IcWarningProduct from './IcWarningProduct.svg';

// OTP
import IcSMS from './IcSMS.svg';
import IcWhatsapp from './IcWhatsapp.svg';
import IcSuccess from './IcSuccess.svg';

// Solid
import IcHome from './IcHome.svg';
import IcHistory from './IcHistory.svg';
import IcCustomer from './IcCustomer.svg';
import IcUser from './IcUser.svg';
import IcBell from './IcBell.svg';
import IcShow from './IcShow.svg';
import IcHide from './IcHide.svg';
import IcStamp from './IcStamp.svg';

import IcPlus from './IcPlus.svg';
import IcSend from './IcSend.svg';
import IcScan from './IcScan.svg';
import IcScanner from './IcScanner.svg';

// Product
import IcPulsa from './IcPulsa.svg';
import IcData from './IcData.svg';
import IcElectric from './IcElectric.svg';
import IcVoucher from './IcVoucher.svg';
import IcIndihome from './IcIndihome.svg';
import IcWater from './IcWater.svg';
import IcHealth from './IcHealth.svg';
import IcMore from './IcMore.svg';
import IcKredit from './IcKredit.svg';
import IcToken from './IcToken.svg';
import IcCall from './IcCall.svg';
import IcPulsaTransfer from './IcPulsaTransfer.svg';
import IcWifiId from './IcWifiId.svg';
import IcInsurance from './IcInsurance.svg';
import IcTelephone from './IcTelephone.svg';
import IcPascabayar from './IcPascabayar.svg';
import IcTax from './IcTax.svg';
import IcNaturalGas from './IcNaturalGas.svg';
import IcWallet from './IcWallet.svg';
import IcMultifinance from './IcMultifinance.svg';
import IcToll from './IcToll.svg';

// Process
import IcProcess from './IcProcess.svg';
import IcFailed from './IcFailed.svg';
import IcSucess from './IcSucess.svg';

// Sort and Filter
import IcSort from './IcSort.svg';
import IcFilter from './IcFilter.svg';

// More
import IcErrorCircle from './IcErrorcircle.svg';
import IcClose from './IcClose.svg';
import IcCheck from './IcCheck.svg';
import IcCopy from './IcCopy.svg';
import IcCopyLine from './IcCopyLine.svg';
import IcPrint from './IcPrint.svg';
import IcShare from './IcShare.svg';
import IcPhoneBook from './IcPhoneBook.svg';
import IcReffund from './IcReffund.svg';
import IcEdit from './IcEdit.svg';
import IcSearch from './IcSearch.svg';
import IcFile from './IcFile.svg';
import IcDiscount from './IcDiscount.svg';
import IcContact from './IcContact.svg';
import IcCamera from './IcCamera.svg';
import IcBag from './IcBag.svg';
import IcAngelRight from './IcAngelRight.svg';
import IcEclipse from './IcEclipse.svg';
import IcHeartOutline from './IcHeartOutline.svg';
import IcHeartFilled from './IcHeartFilled.svg';
import IcAdd from './IcAdd.svg';
import IcNull from './IcNull.svg';
import IcSad from './IcSad.svg';

// Profile
import IcQRCode from './IcQRCode.svg';
import IcRefferal from './IcRefferal.svg';
import IcPoint from './IcPoint.svg';
import IcLock from './IcLock.svg';
import IcHelp from './IcHelp.svg';
import IcReward from './IcReward.svg';
import IcLamp from './IcLamp.svg';
import IcDocs from './IcDocs.svg';
import IcShield from './IcShield.svg';
import IcExclamation from './IcExclamation.svg';

// Admin
import IcWhatsappAdmin from './IcWhatsappAdmin.svg';
import IcTelegramAdmin from './IcTelegramAdmin.svg';

// Tab
import IcProfile from './IcProfile.svg';
import IcNotif from './IcNotif.svg';
import IcApp from './IcApp.svg';
import IcBook from './IcBook.svg';

export {
  IcChevronDown,
  IcChevronLeft,
  IcChevronRight,
  IcChevronUp,
  IcArrowDown,
  IcArrowLeft,
  IcArrowRight,
  IcArrowUp,
  IcWarning,
  IcError,
  IcClose,
  IcSMS,
  IcWhatsapp,
  IcSuccess,
  IcHome,
  IcHistory,
  IcCustomer,
  IcUser,
  IcBell,
  IcHide,
  IcShow,
  IcStamp,
  IcPlus,
  IcScan,
  IcSend,
  IcPulsa,
  IcData,
  IcElectric,
  IcHealth,
  IcIndihome,
  IcMore,
  IcVoucher,
  IcWater,
  IcKredit,
  IcToken,
  IcCall,
  IcPulsaTransfer,
  IcWifiId,
  IcInsurance,
  IcNaturalGas,
  IcPascabayar,
  IcTax,
  IcTelephone,
  IcMultifinance,
  IcWallet,
  IcToll,
  IcCheck,
  IcFailed,
  IcProcess,
  IcSucess,
  IcCopy,
  IcPrint,
  IcShare,
  IcErrorCircle,
  IcPhoneBook,
  IcScanner,
  IcReffund,
  IcSort,
  IcFilter,
  IcEdit,
  IcSearch,
  IcFile,
  IcQRCode,
  IcRefferal,
  IcDocs,
  IcHelp,
  IcLamp,
  IcLock,
  IcPoint,
  IcReward,
  IcShield,
  IcCopyLine,
  IcDiscount,
  IcContact,
  IcTelegramAdmin,
  IcWhatsappAdmin,
  IcCamera,
  IcErrorProduct,
  IcWarningProduct,
  IcProfile,
  IcNotif,
  IcApp,
  IcBook,
  IcBag,
  IcAngelRight,
  IcEclipse,
  IcHeartOutline,
  IcHeartFilled,
  IcAdd,
  IcNull,
  IcSad,
  IcExclamation,
};
