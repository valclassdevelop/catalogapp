import React, {forwardRef, useEffect, useState} from 'react';
import {
  Pressable,
  StyleSheet,
  TextInput as Input,
  TouchableOpacity,
  View,
} from 'react-native';
import Animated, {BounceIn, Transition} from 'react-native-reanimated';
import {
  IcChevronDown,
  IcErrorCircle,
  IcPhoneBook,
  IcSearch,
  IcWarning,
} from '../assets';
import {colors, scaleFont, scaleSize} from '../utils';
import ErrorMessage from './ErrorMessage';
import Gap from './Gap';
import {TextRegular} from './Text';
import moment from 'moment';

const TextInput = forwardRef((props, ref) => {
  const [borderColor, setBorderColor] = useState(colors['RitualCyan-300']);
  const [isActive, setIsActive] = useState(false);

  const onFocus = () => {
    setIsActive(true);
    setBorderColor(colors['DeepDenim-550']);
  };

  const onBlur = () => {
    setIsActive(false);
    setBorderColor(colors['RitualCyan-300']);
  };

  useEffect(() => {
    if (props.isError) {
      setIsActive(false);
      setBorderColor(colors['Danger-Main']);
    }
    setIsActive(false);
    setBorderColor(colors['RitualCyan-300']);
  }, [props.isError]);

  switch (props.type) {
    case 'phone':
      return (
        <>
          <TextRegular
            type="Text Regular 14"
            text={props.label}
            color={colors.Black}
          />
          <Gap height={4} />
          <View style={styles.border(isActive)}>
            <View style={styles.container(borderColor)}>
              <Input
                {...props}
                ref={ref}
                style={styles.input(props.disabled)}
                placeholderTextColor={colors['RitualCyan-500']}
                onFocus={onFocus}
                onBlur={onBlur}
              />
              {props.isError && (
                <Animated.View
                  layout={Transition}
                  entering={BounceIn.delay(500)}>
                  <IcWarning />
                </Animated.View>
              )}
            </View>
          </View>
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
        </>
      );
    case 'phone-with-prefix':
      return (
        <>
          <TextRegular
            type="Text Regular 14"
            text={props.label}
            color={colors.Black}
          />
          <Gap height={4} />
          <View style={styles.border(isActive)}>
            <View
              style={[
                styles.container(borderColor),
                {
                  backgroundColor: props.editable
                    ? colors.White
                    : colors['RitualCyan-100'],
                },
              ]}>
              <TextRegular
                type="Text Regular 16"
                text="+62"
                color={colors['RitualCyan-600']}
              />
              <View style={styles.line} />
              <Input
                {...props}
                ref={ref}
                style={styles.inputPhone(props.editable)}
                placeholderTextColor={colors['RitualCyan-500']}
                onFocus={onFocus}
                onBlur={onBlur}
              />
            </View>
          </View>
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
        </>
      );
    case 'phonebook':
      return (
        <>
          <TextRegular
            type="Text Regular 14"
            text={props.label}
            color={colors.Black}
          />
          <Gap height={4} />
          <View style={styles.border(isActive)}>
            <View style={styles.container(borderColor)}>
              <Input
                {...props}
                ref={ref}
                style={styles.input(props.disabled)}
                placeholderTextColor={colors['RitualCyan-500']}
                onFocus={onFocus}
                onBlur={onBlur}
              />
              {props.value && (
                <Pressable onPress={props.onReset}>
                  <Animated.View
                    layout={Transition}
                    entering={BounceIn.delay(200)}>
                    <IcErrorCircle />
                  </Animated.View>
                </Pressable>
              )}
              <Gap width={12} />
              <Animated.View layout={Transition} entering={BounceIn.delay(100)}>
                <IcPhoneBook />
              </Animated.View>
            </View>
          </View>
        </>
      );
    case 'verify':
      return (
        <>
          <View style={styles.border(isActive)}>
            <View style={styles.containerVerify(borderColor)}>
              <Input
                {...props}
                ref={ref}
                style={styles.inputVerify}
                onFocus={onFocus}
                onBlur={onBlur}
                maxLength={1}
                textAlign="center"
                secureTextEntry={true}
              />
            </View>
          </View>
          <Gap height={4} />
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
        </>
      );
    case 'without-label':
      return (
        <>
          <View style={styles.border(isActive)}>
            <View style={styles.container(borderColor)}>
              <Input
                {...props}
                ref={ref}
                style={styles.input(props.disabled)}
                placeholderTextColor={colors['RitualCyan-500']}
                onFocus={onFocus}
                onBlur={onBlur}
              />
              {props.isError && (
                <Animated.View
                  layout={Transition}
                  entering={BounceIn.delay(500)}>
                  <IcWarning />
                </Animated.View>
              )}
            </View>
          </View>
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
        </>
      );
    case 'textarea':
      return (
        <>
          <TextRegular
            type="Text Regular 14"
            text={props.label}
            color={colors.Black}
          />
          <Gap height={4} />
          <View style={styles.border(isActive)}>
            <View style={styles.containerTextarea(borderColor)}>
              <Input
                {...props}
                ref={ref}
                style={styles.textarea}
                placeholderTextColor={colors['RitualCyan-500']}
                onFocus={onFocus}
                onBlur={onBlur}
                numberOfLines={100}
                textAlignVertical="top"
                blurOnSubmit
                multiline
              />
            </View>
          </View>
        </>
      );
    case 'search':
      return (
        <View style={styles.border(isActive)}>
          <View style={styles.container(borderColor)}>
            <IcSearch stroke={colors['DeepDenim-550']} />
            <Gap width={8} />
            <Input
              {...props}
              ref={ref}
              style={styles.input(props.disabled)}
              placeholderTextColor={colors['RitualCyan-500']}
              onFocus={onFocus}
              onBlur={onBlur}
            />
          </View>
        </View>
      );
    case 'date':
      return (
        <>
          <TextRegular
            type="Text Regular 14"
            text={props.label}
            color={colors.Black}
          />
          <Gap height={4} />
          <View style={styles.border(isActive)}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.container(borderColor)}
              onPress={props.onDate}>
              <TextRegular
                type="Text Regular 16"
                text={moment(props.value).format('DD-MM-YYYY')}
                color={colors.Black}
              />
            </TouchableOpacity>
          </View>
        </>
      );
    case 'select':
      return (
        <>
          <TextRegular
            type="Text Regular 14"
            text={props.label}
            color={colors.Black}
          />
          <Gap height={4} />
          <View style={styles.border(isActive)}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.containerSelect}
              onPress={props.onSelect}>
              <TextRegular
                type="Text Regular 16"
                text={props.value}
                color={colors.Black}
              />
              <IcChevronDown
                width={20}
                height={20}
                stroke={colors['RitualCyan-500']}
              />
            </TouchableOpacity>
          </View>
        </>
      );
    default:
      return (
        <>
          <TextRegular
            type="Text Regular 14"
            text={props.label}
            color={colors.Black}
          />
          <Gap height={4} />
          <View style={styles.border(isActive)}>
            <View
              style={[
                styles.container(borderColor),
                {
                  backgroundColor: !props.disabled
                    ? colors.White
                    : colors['RitualCyan-100'],
                },
              ]}>
              <Input
                {...props}
                ref={ref}
                style={styles.input(props.disabled)}
                placeholderTextColor={colors['RitualCyan-500']}
                onFocus={onFocus}
                onBlur={onBlur}
                editable={!props.disabled}
              />
              {props.isError && (
                <Animated.View
                  layout={Transition}
                  entering={BounceIn.delay(500)}>
                  <IcWarning />
                </Animated.View>
              )}
            </View>
          </View>
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
        </>
      );
  }
});

export default React.memo(TextInput);

const styles = StyleSheet.create({
  container: borderColor => ({
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: scaleSize(50),
    paddingHorizontal: scaleSize(16),
    paddingVertical: scaleSize(10),
    borderWidth: 1,
    borderColor,
    borderRadius: scaleSize(12),
  }),
  containerTextarea: borderColor => ({
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: scaleSize(150),
    paddingHorizontal: scaleSize(16),
    paddingVertical: scaleSize(10),
    borderWidth: 1,
    borderColor,
    borderRadius: scaleSize(12),
  }),
  containerVerify: borderColor => ({
    flexDirection: 'row',
    alignItems: 'center',
    width: scaleSize(60),
    height: scaleSize(56),
    padding: scaleSize(16),
    borderWidth: 1,
    borderColor,
    borderRadius: scaleSize(12),
  }),
  containerSelect: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: scaleSize(50),
    paddingHorizontal: scaleSize(16),
    paddingVertical: scaleSize(10),
    borderWidth: 1,
    borderColor: colors['RitualCyan-300'],
    borderRadius: scaleSize(12),
  },
  border: isActive => ({
    borderWidth: 2,
    borderRadius: scaleSize(14),
    borderColor: isActive ? colors['DeepDenim-100'] : 'transparent',
  }),
  row: {
    flexDirection: 'row',
  },
  input: disabled => ({
    flex: 1,
    height: scaleSize(50),
    fontFamily: 'Ubuntu-Regular',
    fontSize: scaleFont(16),
    // color: disabled ? colors['RitualCyan-500'] : colors.Black,
    lineHeight: 20,
  }),
  inputPhone: editable => ({
    flex: 1,
    height: scaleSize(50),
    fontFamily: 'Ubuntu-Regular',
    fontSize: scaleFont(16),
    color: editable ? colors.Black : colors['RitualCyan-500'],
    lineHeight: 20,
  }),
  textarea: {
    flex: 1,
    height: scaleSize(150),
    fontFamily: 'Ubuntu-Regular',
    fontSize: scaleFont(16),
    color: colors.Black,
    lineHeight: 20,
  },
  inputVerify: {
    flex: 1,
    height: scaleSize(50),
    fontFamily: 'Ubuntu-Medium',
    fontSize: scaleFont(16),
    color: colors.Black,
    lineHeight: 20,
  },
  line: {
    width: 1,
    height: scaleSize(20),
    backgroundColor: colors['RitualCyan-300'],
    marginHorizontal: scaleFont(10),
  },
});
