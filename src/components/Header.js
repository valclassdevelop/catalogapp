import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors, scaleSize} from '../utils';
import {IcBag, IcExclamation} from '../assets';
import Gap from './Gap';
import {TextBold} from './Text';
import {useSelector} from 'react-redux';

const Header = ({
  text,
  textColor = colors.Black,
  iconColor = colors.Black,
  type,
  backgroundColor = 'transparent',
  onPressRight,
  onPress,
}) => {
  const [inData, setInData] = useState();

  const {cart} = useSelector(state => state.product);

  useEffect(() => {
    setInData(cart?.length);
  }, [cart]);

  return type === 'main' ? (
    <View style={styles.headerMain}>
      <View style={styles.headerLeft}>
        <Text style={styles.topTitle}>{text}</Text>
        <Gap height={6} />
        <TextBold color={colors.White} text="Windmill" type="Title Bold 18" />
      </View>
      <TouchableOpacity activeOpacity={0.8} onPress={onPressRight}>
        <IcBag onPress={onPress} width={24} height={24} stroke={colors.White} />
        <View style={styles.index}>
          <Text style={styles.indexText}>{inData}</Text>
        </View>
      </TouchableOpacity>
    </View>
  ) : (
    <View style={styles.headerMain}>
      <View style={styles.headerLeft}>
        <Text style={styles.topTitle}>{text}</Text>
        <Gap height={6} />
        <TextBold color={colors.White} text="Windmill" type="Title Bold 18" />
      </View>
      <View style={styles.warning}>
        <TouchableOpacity activeOpacity={0.6} onPress={onPressRight}>
          <IcExclamation
            onPress={onPress}
            width={12}
            height={12}
            stroke={colors.White}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default React.memo(Header);

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'baseline',
    zIndex: 1,
    fontFamily: 'Poppins-Black',
  },
  headerMain: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.primary,
    height: 'auto',
    paddingTop: scaleSize(20),
    paddingBottom: scaleSize(16),
    alignItems: 'center',
    paddingHorizontal: scaleSize(20),
    elevation: 3,
  },
  backButton: {
    width: scaleSize(24),
    height: scaleSize(24),
  },
  headerLeft: {
    justifyContent: 'center',
    alignSelf: 'baseline',
  },
  topTitle: {
    color: colors.purple,
  },
  index: {
    minWidth: scaleSize(16),
    minHeight: scaleSize(16),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.purple,
    position: 'absolute',
    borderRadius: 99,
    top: -4,
    right: 0,
  },
  indexText: {
    color: colors.White,
    fontSize: 8,
  },
  warning: {
    borderRadius: 99,
    backgroundColor: colors.White,
    alignItems: 'center',
    justifyContent: 'center',
    width: scaleSize(24),
    height: scaleSize(24),
  },
});
