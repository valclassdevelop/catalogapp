import React from 'react';
import Animated, {BounceInLeft, Transition} from 'react-native-reanimated';
import {colors} from '../utils';
import {TextRegular} from './Text';

const ErrorMessage = ({text, isError}) =>
  isError ? (
    <Animated.View entering={BounceInLeft.duration(1000)} layout={Transition}>
      <TextRegular
        type="Text Regular 10"
        text={text}
        color={colors['Danger-Main']}
      />
    </Animated.View>
  ) : null;

export default React.memo(ErrorMessage);
