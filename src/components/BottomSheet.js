import React from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import {colors, scaleSize} from '../utils';
import Modal from 'react-native-modal';

const BottomSheet = ({children, isVisible, onClose, height = 'auto'}) => {
  return (
    <Modal
      useNativeDriver
      isVisible={isVisible}
      style={styles.container}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      animationInTiming={800}
      animationOutTiming={400}
      backdropColor={colors.Black}
      backdropOpacity={0.6}
      onBackdropPress={() => {
        onClose();
      }}
      onBackButtonPress={() => {
        onClose();
      }}
      transparent={true}
      hideModalContentWhileAnimating={true}>
      <View style={[styles.wrapper, {height: height}]}>{children}</View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 0,
    justifyContent: 'flex-end',
  },
  wrapper: {
    top: 22,
    backgroundColor: colors.White,
    borderTopLeftRadius: scaleSize(16),
    borderTopRightRadius: scaleSize(16),
  },
});

export default React.memo(BottomSheet);
