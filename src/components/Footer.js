import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {TextBold, TextRegular} from './Text';
import {colors} from '../utils';
import Gap from './Gap';

const Footer = ({text, action, onPress}) => {
  return (
    <View style={styles.footer}>
      <View style={styles.row}>
        <TextRegular type="Text Regular 16" text={text} color={colors.Black} />
        <Gap width={20} />
        <View style={styles.line} />
        <Gap width={20} />
        <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
          <TextBold
            type="Text Bold 16"
            text={action}
            color={colors['DeepDenim-550']}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default React.memo(Footer);

const styles = StyleSheet.create({
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    width: 1.5,
    height: 12,
    backgroundColor: colors['RitualCyan-300'],
  },
});
