import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {scaleFont} from '../utils';

const getSize = type => {
  switch (type) {
    case 'Title Bold 36':
      return 36;
    case 'Title Bold 32':
      return 32;
    case 'Title Bold 24':
      return 24;
    case 'Title Bold 20':
      return 20;
    case 'Title Bold 18':
      return 18;
    case 'Text Regular 16':
      return 16;
    case 'Text Regular 14':
      return 14;
    case 'Text Regular 12':
      return 12;
    case 'Text Regular 10':
      return 10;
    case 'Text SemiBold 16':
      return 16;
    case 'Text SemiBold 12':
      return 12;
    case 'Text Medium 18':
      return 18;
    case 'Text Medium 16':
      return 16;
    case 'Text Medium 14':
      return 14;
    case 'Text Medium 12':
      return 12;
    case 'Text Medium 10':
      return 10;
    case 'Text Bold 24':
      return 24;
    case 'Text Bold 18':
      return 18;
    case 'Text Bold 16':
      return 16;
    case 'Text Bold 14':
      return 14;
    case 'Text Bold 12':
      return 12;
    case 'Text Bold 10':
      return 10;
    default:
      return 16;
  }
};

const getLineHeight = type => {
  switch (type) {
    case 'Title Bold 36':
      return 50;
    case 'Title Bold 32':
      return 45;
    case 'Title Bold 24':
      return 40;
    case 'Title Bold 20':
      return 25;
    case 'Title Bold 18':
      return 20;
    case 'Text Regular 16':
      return 20;
    case 'Text Regular 14':
      return 20;
    case 'Text Regular 12':
      return 18;
    case 'Text Regular 10':
      return 12;
    case 'Text SemiBold 16':
      return 18.38;
    case 'Text SemiBold 12':
      return 20;
    case 'Text Medium 18':
      return 22;
    case 'Text Medium 16':
      return 20;
    case 'Text Medium 14':
      return 20;
    case 'Text Medium 12':
      return 14;
    case 'Text Medium 10':
      return 12;
    case 'Text Bold 24':
      return 20;
    case 'Text Bold 18':
      return 20;
    case 'Text Bold 16':
      return 20;
    case 'Text Bold 14':
      return 20;
    case 'Text Bold 12':
      return 16;
    case 'Text Bold 10':
      return 12;
    default:
      return 20;
  }
};

export const TextBold = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(type);
  return (
    <Text style={[styles.bold(size, color, lineHeight), style]}>{text}</Text>
  );
};

export const TextMedium = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(type);
  return (
    <Text style={[styles.medium(size, color, lineHeight), style]}>{text}</Text>
  );
};

export const TextRegular = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(type);
  return (
    <Text style={[styles.regular(size, color, lineHeight), style]}>{text}</Text>
  );
};

const styles = StyleSheet.create({
  bold: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Bold',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
  medium: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Medium',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
  regular: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Regular',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
});
