import {ActivityIndicator, StyleSheet, View} from 'react-native';
import React from 'react';
import {colors} from '../utils';

const Loading = () => {
  return (
    <View style={styles.page}>
      <ActivityIndicator size="large" color={colors['DeepDenim-550']} />
    </View>
  );
};

export default Loading;

const styles = StyleSheet.create({
  page: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.5)',
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
