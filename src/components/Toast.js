import React, {useEffect} from 'react';
import {Dimensions, StyleSheet, TouchableOpacity, View} from 'react-native';
import Modal from 'react-native-modal';
import {IcClose, IcError} from '../assets';
import {colors, scaleSize} from '../utils';
import Gap from './Gap';
import {TextRegular} from './Text';

const width = Dimensions.get('window').width;

const Toast = ({isVisible, onClose, text}) => {
  useEffect(() => {
    if (isVisible) {
      setTimeout(() => {
        onClose();
      }, 4000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isVisible]);

  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={() => {
        onClose();
      }}
      onBackButtonPress={() => {
        onClose();
      }}
      backdropColor={'rgba(0,0,0,0)'}
      animationIn="slideInUp"
      animationInTiming={1000}
      animationOutTiming={500}
      animationOut="fadeOut"
      style={styles.modal}
      useNativeDriver
      hideModalContentWhileAnimating>
      <View style={styles.container}>
        <IcError fill={colors['Danger-Main']} />
        <Gap width={8} />
        <TextRegular
          type="Text 14"
          text={text}
          color={colors['Danger-Main']}
          style={styles.width}
        />
        <Gap width={8} />
        <TouchableOpacity
          onPress={() => {
            onClose();
          }}>
          <IcClose stroke={colors['Neutral-100']} width={20} height={20} />
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

export default React.memo(Toast);

const styles = StyleSheet.create({
  modal: {margin: 0, justifyContent: 'flex-end'},
  container: {
    backgroundColor: colors['Danger-Surface'],
    padding: scaleSize(12),
    marginBottom: (width * 20) / 100,
    marginHorizontal: scaleSize(16),
    borderRadius: scaleSize(8),
    borderWidth: 1,
    borderColor: colors['Danger-Main'],
    flexDirection: 'row',
    alignItems: 'center',
  },
  width: {
    width: '83%',
  },
});
