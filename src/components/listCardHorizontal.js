import React, {useState} from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import {useDispatch} from 'react-redux';
import {IcAdd} from '../assets';
import {colors, NumberFormatter, scaleSize} from '../utils';
import Gap from './Gap';
import {TextBold, TextRegular} from './Text';

const ListCardHorizontal = ({
  title,
  price,
  imageLink,
  description,
  onPressPrice,
}) => {
  const [active, setActive] = useState(false);
  const dispatch = useDispatch();

  const addCart = (name, price, image, description) => {
    setActive(true);
    setTimeout(() => {
      setActive(false);
    }, 100);
    let num = 5;
    //  alert(num);
    let _id = '';
    let possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < num; i++) {
      _id += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    const data = {
      _id,
      name,
      price,
      image,
      description,
    };
    dispatch({
      type: 'ADD_CART',
      value: data,
    });
  };

  return (
    <TouchableOpacity activeOpacity={0.8}>
      <View style={styles.card}>
        <View style={styles.cardLeft}>
          <TouchableOpacity onPress={onPressPrice}>
            <Image
              source={{
                uri: imageLink,
              }}
              style={{width: 42, height: 42, borderRadius: scaleSize(6)}}
            />
          </TouchableOpacity>
          <Gap width={5} />
          <View style={styles.cardLeftDesc}>
            <TextRegular
              text={title}
              type="Text Regular 16"
              color={colors.White}
              style={styles.titleCard}
            />
            <TextBold
              text={NumberFormatter(price, '$ ')}
              type="Text Bold 14"
              color={colors.White}
              style={styles.priceCard}
            />
          </View>
        </View>
        <View style={styles.cardRight}>
          <View style={styles.add(active)}>
            <TouchableOpacity activeOpacity={0.6}>
              <IcAdd
                width={18}
                height={18}
                onPress={() => addCart(title, price, imageLink, description)}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <Gap height={20} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  add: active => ({
    borderRadius: 99,
    backgroundColor: active ? colors.purple : colors.White,
    alignItems: 'center',
    width: scaleSize(24),
    height: scaleSize(24),
    padding: scaleSize(6),
    justifyContent: 'center',
    fontFamily: 'Poppins-Regular',
  }),
  card: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    fontFamily: 'Poppins-Regular',
  },
  cardCart: {
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: scaleSize(12),
    flexDirection: 'row',
    fontFamily: 'Poppins-Regular',
  },
  cardLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontFamily: 'Poppins-Regular',
  },
  cardLeftDesc: {
    justifyContent: 'space-between',
    flexDirection: 'column',
    fontFamily: 'Poppins-Regular',
  },
  cardRight: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    fontFamily: 'Poppins-Regular',
  },
  titleCard: {
    fontFamily: 'Poppins-Bold',
  },
  priceCard: {
    fontFamily: 'Poppins-Regular',
  },
});

export default ListCardHorizontal;
