import React, {useState} from 'react';
import {Image} from 'react-native';

const ResponsiveImage = ({
  defaultAspectRatio,
  style,
  onLoad,
  ...otherProps
}) => {
  const [aspectRatio, setAspectRatio] = useState(defaultAspectRatio);
  const customStyle = {aspectRatio};

  return (
    <Image
      {...otherProps}
      style={
        Array.isArray(style) ? [...style, customStyle] : [style, customStyle]
      }
      onLoad={e => {
        const source = e.nativeEvent.source;
        setAspectRatio(source.width / source.height);
        onLoad && onLoad(e);
      }}
    />
  );
};

export default ResponsiveImage;
