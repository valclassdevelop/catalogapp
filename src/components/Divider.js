import React from 'react';
import {StyleSheet, View} from 'react-native';
import {colors, scaleSize} from '../utils';

const Divider = ({
  height = scaleSize(5),
  backgroundColor = colors['DeepDenim-50'],
}) => {
  return <View style={styles.divider(height, backgroundColor)} />;
};

export default Divider;

const styles = StyleSheet.create({
  divider: (height, backgroundColor) => ({
    height,
    width: '120%',
    marginHorizontal: scaleSize(-20),
    backgroundColor,
  }),
});
