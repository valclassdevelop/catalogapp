import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {TextBold, TextRegular} from './Text';
import {colors} from '../utils';
import Gap from './Gap';

const ResendButton = ({isResend, text, timer, action, onPress}) => {
  return (
    <View style={styles.footer}>
      <View style={styles.center}>
        <TextRegular type="Text Regular 16" text={text} color={colors.Black} />
        <Gap height={10} />
        {isResend ? (
          <View style={styles.row}>
            <TextBold
              type="Text Bold 16"
              text="Tunggu "
              color={colors['DeepDenim-550']}
            />
            <TextBold
              type="Text Bold 16"
              text={`${timer === 60 ? '01:00' : `00:${timer}`}`}
              color={colors['DeepDenim-550']}
            />
            <TextBold
              type="Text Bold 16"
              text=" detik"
              color={colors['DeepDenim-550']}
            />
          </View>
        ) : (
          <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
            <TextBold
              type="Text Bold 16"
              text={action}
              color={colors['DeepDenim-550']}
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default React.memo(ResendButton);

const styles = StyleSheet.create({
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
