import React from 'react';
import {
  StyleSheet,
  View,
  Animated,
  useWindowDimensions,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image,
} from 'react-native';
import {colors, scaleSize} from '../utils';
import Gap from './Gap';
import {TextBold, TextRegular} from './Text';
import {IcChevronRight} from '../assets';

const OnBoardingContent = ({dots, data, scrollX, scrollTo}) => {
  const {width} = useWindowDimensions();
  return (
    <>
      <View style={[styles.container, {width}]}>
        <Image
          source={data?.image}
          style={[styles.image, {width}]}
          resizeMode="contain"
        />
      </View>
      <ImageBackground source={data?.background} style={styles.background}>
        <TextBold
          type="Title Bold 36"
          text={data?.title}
          color={colors.White}
        />
        <Gap height={10} />
        <TextRegular
          type="Text Regular 16"
          text={data?.description}
          color={colors.White}
        />
        <Gap height={28} />
        <View style={styles.row}>
          <View style={styles.pagination}>
            {dots.map((_, i) => {
              const inputRange = [(i - 1) * width, i * width, i * width];
              const dotWidth = scrollX.interpolate({
                inputRange,
                outputRange: [10, 20, 10],
                extrapolate: 'clamp',
              });
              const opacity = scrollX.interpolate({
                inputRange,
                outputRange: [0.5, 1, 0.5],
                extrapolate: 'clamp',
              });
              return (
                <Animated.View
                  key={i.toString()}
                  style={[styles.dot, {width: dotWidth, opacity}]}
                />
              );
            })}
          </View>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.button}
            onPress={scrollTo}>
            <IcChevronRight stroke={colors['DeepDenim-550']} />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </>
  );
};

export default React.memo(OnBoardingContent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.White,
    paddingTop: scaleSize(80),
  },
  image: {
    height: scaleSize(280),
  },
  background: {
    width: Dimensions.get('window').width,
    height: scaleSize(400),
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: -52,
    paddingHorizontal: scaleSize(20),
    paddingVertical: scaleSize(138),
    backgroundColor: colors.White,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  pagination: {
    flexDirection: 'row',
  },
  dot: {
    height: scaleSize(8),
    borderRadius: scaleSize(8),
    backgroundColor: colors.White,
    marginHorizontal: scaleSize(6),
  },
  button: {
    backgroundColor: colors.White,
    height: scaleSize(48),
    width: scaleSize(48),
    borderRadius: scaleSize(12),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
