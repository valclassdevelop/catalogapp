import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {IcCustomer, IcHistory, IcHome, IcUser} from '../assets';
import {TextMedium, TextRegular} from './Text';
import Gap from './Gap';
import {colors, scaleSize} from '../utils';

const Icon = ({label, active}) => {
  switch (label) {
    case 'HomePage':
      return active ? (
        <IcHome fill={colors.White} />
      ) : (
        <IcHome fill={colors['RitualCyan-400']} />
      );
    case 'History':
      return active ? (
        <IcHistory fill={colors.White} />
      ) : (
        <IcHistory fill={colors['RitualCyan-400']} />
      );
    case 'Notifications':
      return active ? (
        <IcCustomer fill={colors.White} />
      ) : (
        <IcCustomer fill={colors['RitualCyan-400']} />
      );
    case 'Profile':
      return active ? (
        <IcUser fill={colors.White} />
      ) : (
        <IcUser fill={colors['RitualCyan-400']} />
      );

    default:
      return <IcHome fill={colors.White} />;
  }
};

const BottomNavigation = ({state, descriptors, navigation}) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View>
      <View style={styles.container}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              key={index}
              activeOpacity={0.7}
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              style={styles.menu}
              onPress={onPress}
              onLongPress={onLongPress}>
              <Icon label={label} active={isFocused} />
              <Gap height={8} />
              {isFocused ? (
                <TextMedium
                  type="Text Regular 10"
                  text={label}
                  color={colors.White}
                />
              ) : (
                <TextRegular
                  type="Text Regular 10"
                  text={label}
                  color={colors['RitualCyan-400']}
                />
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

export default BottomNavigation;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    backgroundColor: '#282828',
    justifyContent: 'space-between',
    paddingHorizontal: scaleSize(30),
    elevation: 10,
  },
  menu: {
    height: scaleSize(48),
    marginVertical: scaleSize(16),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
