import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {colors, scaleSize} from '../utils';
import Gap from './Gap';
import {TextBold} from './Text';

export const FilledButton = ({
  paddingVertical = scaleSize(16),
  paddingHorizontal = scaleSize(24),
  backgroundColor = colors['DeepDenim-550'],
  borderRadius = scaleSize(12),
  textColor = colors.White,
  fontSize = 16,
  icon,
  text,
  disable,
  onPress,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={[
        styles.button,
        {
          paddingVertical,
          paddingHorizontal,
          backgroundColor: disable ? colors['RitualCyan-100'] : backgroundColor,
          borderRadius,
        },
      ]}
      disabled={disable}
      onPress={onPress}>
      {icon ? (
        icon
      ) : (
        <TextBold
          type={`Text Bold ${fontSize}`}
          text={text}
          color={disable ? colors['RitualCyan-400'] : textColor}
        />
      )}
    </TouchableOpacity>
  );
};

export const OutlineButton = ({
  paddingVertical = scaleSize(16),
  paddingHorizontal = scaleSize(30),
  backgroundColor = colors.White,
  borderRadius = scaleSize(12),
  borderColor = colors['RitualCyan-200'],
  textColor = colors.Black,
  fontSize = 16,
  icon,
  text,
  onPress,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[
        styles.outlineButton,
        {
          paddingVertical,
          paddingHorizontal,
          backgroundColor,
          borderRadius,
          borderColor,
        },
      ]}
      onPress={onPress}>
      {icon}
      {icon && <Gap width={16} />}
      <TextBold type={`Text Bold ${fontSize}`} text={text} color={textColor} />
    </TouchableOpacity>
  );
};

export const TextButton = ({text, onPress}) => {
  return (
    <TouchableOpacity
      style={styles.textButton}
      activeOpacity={0.8}
      onPress={onPress}>
      <TextBold
        type="Text Bold 16"
        text={text}
        color={colors['DeepDenim-550']}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  outlineButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
  },
  textButton: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: scaleSize(12),
    borderRadius: scaleSize(12),
  },
});
